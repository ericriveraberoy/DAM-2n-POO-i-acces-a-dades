package prova;

public class PerDos implements Serie {
	private double nombre;

	public double getNombre() {
		return nombre;
	}

	public void nombreInicial(double d) {
		nombre = d;
	}

	public void inicialitza() {
		nombre = 1;
	}

	public double seguentNombre() {
		return nombre*2;
	}
}
