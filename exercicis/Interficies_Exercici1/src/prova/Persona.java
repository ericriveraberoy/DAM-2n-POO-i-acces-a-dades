package prova;

/*
 Creeu la classe Persona amb els atributs pes, edat i al�ada.
 Implementeu la interf�cie Comparable per fer que es compari l'al�ada
 */

public class Persona implements Comparable<Persona> {
	private double pes;
	private double alcada;
	private int edat;
	
	public double getPes() {
		return pes;
	}
	public void setPes(double pes) {
		this.pes = pes;
	}
	public double getAlcada() {
		return alcada;
	}
	public void setAlcada(double alcada) {
		this.alcada = alcada;
	}
	public int getEdat() {
		return edat;
	}
	public void setEdat(int edat) {
		this.edat = edat;
	}
	
	public int compareTo(Persona p) {
		if (getAlcada() > p.getAlcada())
			return 1;
		else if (getAlcada() < p.getAlcada())
			return -1;
		else return 0;
	}
	
}
