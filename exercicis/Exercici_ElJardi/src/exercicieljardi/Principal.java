package exercicieljardi;

import java.util.Scanner;

public class Principal {

	static int MIDA_JARDI = 20;
	
	public static void main(String[] args) {
		
		// Creo un jardi i un parell tipus de plantes
		Jardi jardi = new Jardi(MIDA_JARDI);
		Altibus a = new Altibus();
		Declinus d = new Declinus();
		
		// Planto llavors dels tipus de plantes al meu jardi
		jardi.plantaLlavor(a, 2);
		jardi.plantaLlavor(d, 7);
		jardi.plantaLlavor(a, 12);
		jardi.plantaLlavor(d, 16);
		jardi.plantaLlavor(a, 19);
		
		// Faig passar el temps fins que l'usuari escriu "surt"
		String s = new String();
		Scanner sc = new Scanner (System.in);        
		do {
			// Faig passar el temps i dibuixo el jardi
			jardi.temps();
			System.out.print(jardi.toString());
			
			// [Opcional] Per cada vegada que dibuixi un jardi, possa-li un "terra"
			for (int i = 0; i < MIDA_JARDI; i++) System.out.print("_");
			
			// Demano a l'usuari que escrigui
			s = sc.nextLine ();
		} while (!s.equals("surt"));
		sc.close();
	}

}
