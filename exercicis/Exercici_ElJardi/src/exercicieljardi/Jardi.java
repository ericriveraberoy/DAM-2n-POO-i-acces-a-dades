package exercicieljardi;

public class Jardi {
	Planta jardi[];
	int mida = 10;

	public Jardi(int m) {
		if (m > 0) {
			mida = m;
		} else {
			mida = 10;
		}
		jardi = new Planta[mida];
	}
	
	public void temps() {
		Planta p;
		for (int i=0; i < jardi.length; i++) { // Per cada posicio del jardi...
			if (jardi[i] != null) { // Si hi ha una planta, es cridar� al seu m�tode creix().
				p = jardi[i].creix();
				if (p instanceof Llavor) { // Si el m�tode creix() ha retornat una Llavor...
					int posicioLlavor = i + jardi[i].escampaLlavor(); // Agafa la posicio on ha caigut...
 					if (posicioLlavor >= 0 && posicioLlavor < jardi.length) { // Si la llavor ha caigut dins del jardi...
 						if (jardi[posicioLlavor] == null) { // Comprova si no hi ha cap planta ja.
 							jardi[posicioLlavor] = p; // Si no n'hi ha, fica la llavor en la posicio on ha caigut.
 						}
 					}
				} else if (p instanceof Planta) { // En canvi, si el m�tode creix() ha retornat una Planta
					jardi[i] = p; // Reemplaza la planta actual per la retornada
				}
				
				if (jardi[i].esViva == false) { // Finalment, comprova si la planta est� viva.
					jardi[i] = null; // Si no est� viva, neteja l'espai del jardi.
				}
			}
		}
	}
	
	public String toString() {
		String s = new String();
		for (int nivell = 11; nivell > 0; nivell--) { // (nivell = fila)
			for (int col = 0; col < mida; col++) {
				if (jardi[col] != null) {
					s += jardi[col].getChar(nivell);
				} else {
					s += " ";
				}
			}
			s += "\n";
		}
		return s;
	}
	
	public boolean plantaLlavor(Planta p, int pos) {
		if (jardi[pos] == null) {
			jardi[pos] = p;
			return true;
		} else {
			return false;
		}
	}
}
