package exercicieljardi;

public class Llavor extends Planta {
	Planta planta;
	int temps = 0;
	
	public Llavor(Planta planta) {
		if (planta instanceof Llavor) {
			throw new IllegalArgumentException();
		} else {
			this.planta = planta;
		}
	}

	public Planta creix() {
		if (temps < 5) {
			temps++;
			return null;
		} else {
			return planta;
		}
	}
	
	@Override
	public char getChar(int nivell) {
		if (nivell == 0) {
			return ".".charAt(0);
		} else {
			return " ".charAt(0);
		}
	}
}
