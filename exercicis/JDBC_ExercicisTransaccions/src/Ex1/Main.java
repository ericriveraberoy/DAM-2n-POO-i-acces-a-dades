package Ex1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Scanner;

public class Main {

	private Connection connection = null;
    private Statement statement = null;
    private Scanner scanner = new Scanner(System.in);

    /*
     * Sentencies SQL
     */
    String sqlNextEmpNo = "SELECT max(emp_no)+1 FROM employees;";
    String sqlDepartments = "SELECT dept_no, dept_name FROM departments;";
    String sqlInsertEmployee = "INSERT INTO employees(emp_no, first_name, last_name, gender, hire_date)"
    							+ "VALUES(?, ?, ?, ?, now());";
    String sqlInsertaDeptEmp = "INSERT INTO dept_emp(emp_no, dept_no, from_date, to_date) VALUES(?, ?, now(), '9999-01-01');";
    String sqlInsertaSalary = "INSERT INTO salaries(emp_no, salary, from_date, to_date) VALUES(?, ?, now(), '9999-01-01');";
    String sqlInsertaTitle = "INSERT INTO titles (emp_no, title, from_date, to_date) VALUES (?, ?, now(), '9999-01-01');";
    
    public void run() throws SQLException {
    	System.out.println("  [DONA D'ALTA UN NOU EMPLEAT]\n");
    	int empNo = 1;
    	
    	/*
    	 *  Demanem les dades
    	 */
    	System.out.println("Nom: ");
    	String nom = scanner.nextLine();
    	
    	System.out.println("Cognom: ");
    	String cognom = scanner.nextLine();
    	
    	String genere = "X";
    	while (!genere.equals("M") && !genere.equals("F")) {
    		System.out.println("Genere (M/F): ");
        	genere = scanner.nextLine();
    	}
    	
    	System.out.println("Departament: ");
    	String departament = scanner.nextLine();
    	
    	System.out.println("Titol: ");
    	String titol = scanner.nextLine();
    	
    	int sou = 0;
    	while (sou <= 0) {
    		System.out.println("Sou (ha de ser major de 0): ");
        	sou = scanner.nextInt();
    	}
    	
    	/*
		    1 - Afegir una fila a la taula employees amb les dades de l'empleat. La data de contractaci� �s la data actual en el moment d'executar el programa.
		    2 - Afegir una fila a la taula dept_emp per assignar el nou empleat al seu departament.
		    3 - Afegir una fila a la taula salaries per assignar-li el seu sou.
		    4 - Afegir una fila a la taula titles per assignar-li el seu t�tol.
    	*/
    	try (Connection con = DriverManager.getConnection(
                "jdbc:mariadb://localhost:3306/employees", "root", "")) {
    		try (PreparedStatement findNextEmpNoSt = con.prepareStatement(sqlNextEmpNo);
    				PreparedStatement findDepartmentsSt = con.prepareStatement(sqlDepartments);
    				PreparedStatement insertEmployeeSt = con.prepareStatement(sqlInsertEmployee);
    				PreparedStatement insertDeptEmpSt = con.prepareStatement(sqlInsertaDeptEmp);
    				PreparedStatement insertSalarySt = con.prepareStatement(sqlInsertaSalary);
    				PreparedStatement insertTitleSt = con.prepareStatement(sqlInsertaTitle);) {
    			
    			// Desactivem autocommit per iniciar transacci�
                con.setAutoCommit(false);
                
                // Cerquem el emp_no que li pertoca
                try (ResultSet rs = findNextEmpNoSt.executeQuery()) {
                	if (rs.next()) {
                        empNo = rs.getInt(1);
                    }
                }
                
                // Cerquem els departaments
                HashMap<String, String> departments = new HashMap<String, String>();
                try (ResultSet rs = findDepartmentsSt.executeQuery()) {
                	while (rs.next()) {
                        departments.put(rs.getString(2), rs.getString(1)); // dept_name, dept_no
                    }
                }
                while (!departments.containsKey(departament)) {
                	System.out.println("El departament escollit no existeis! Escriu un departament valid: ");
                	departament = scanner.nextLine();
                }
                
                // Insertem l'empleat
                insertEmployeeSt.setInt(1, empNo);
                insertEmployeeSt.setString(2, nom);
                insertEmployeeSt.setString(3, cognom);
                insertEmployeeSt.setString(4, genere);
                insertEmployeeSt.executeUpdate();
                
                // Insertem l'empleat al departament corresponent
                insertDeptEmpSt.setInt(1, empNo);
                insertDeptEmpSt.setString(2, departments.get(departament));
                insertDeptEmpSt.executeUpdate();
                
                // Insertem el salari
                insertSalarySt.setInt(1, empNo);
                insertSalarySt.setInt(2, sou);
                insertSalarySt.executeUpdate();
                
                // Insertem el titol
                insertTitleSt.setInt(1, empNo);
                insertTitleSt.setString(2, titol);
                insertTitleSt.executeUpdate();
                
                con.commit();
                System.out.println("Transaccio realitzada!");
    		} catch (SQLException e) {
                // En cas d'error, tornem la base de dades al seu estat inicial
                System.err.print("S'ha produ�t una errada a la transacci�, desfem els canvis");
                con.rollback();
            } finally {
                // Finalment reactivem l'autocommit
                con.setAutoCommit(true);
            }
    	}
    }
    

    public static void main(String[] args) throws SQLException {
        Main ex = new Main();
        ex.run();
    }

}
