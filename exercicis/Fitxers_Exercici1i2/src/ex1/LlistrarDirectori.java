package ex1;

import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.AclFileAttributeView;
import java.nio.file.attribute.PosixFileAttributeView;
import java.util.ArrayList;

/*
	Ex1:
	Fes un programa que rebi un nom de directori com a par�metre i mostri el seu contingut,
	indicant en cada cas si es tracta d'un fitxer o directori i els permisos que tenim sobre ell.
	La sortida tindr� un aspecte similar a aquest:
		-rw- fitxer
		drwx directori
		...
	
	Ex2:
	Fes un programa que rebi un nom de directori com a par�metre i mostri el seu contingut,
	indicant en cada cas si es tracta d'un fitxer o directori i els permisos que tenim sobre ell.
	El programa actuar� recursivament, mostrant el contingut de tots els subdirectoris amb qu� es vagi trobant.
	Per fer-ho, utilitza una pila on es guardin els noms de directoris pendents de mostrar.
 */

public class LlistrarDirectori {
	public static void main(String[] args) {
		
		ArrayList<String> arrayStringDirs = new ArrayList<String>();
		
		if (args.length == 1) {
			arrayStringDirs.add(args[0]);
			
			while (!arrayStringDirs.isEmpty()) {
				Path dir = Paths.get(arrayStringDirs.get(0));
	            System.out.println("Fitxers del directori " + dir + ":");
				if (Files.isDirectory(dir)) {
					try(DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
						for (Path fitxer: stream) {
							if (Files.isDirectory(fitxer)) {
								arrayStringDirs.add(fitxer.toString());
							}
							if(fitxer.toFile().canRead()) System.out.print("r");
							else System.out.print("-");
							
							if(fitxer.toFile().canWrite()) System.out.print("w");
							else System.out.print("-");
							
							if(fitxer.toFile().canExecute()) System.out.print("x");
							else System.out.print("-");
							
							System.out.print("   " + fitxer.getFileName() + "\n");
	                    }
					} catch(IOException | DirectoryIteratorException ex) {
						System.err.println(ex);
					}
				} else {
					System.err.println(dir.toString() + " no �s un directori");
				}
				arrayStringDirs.remove(0);
			}
		} else {
			System.err.println("�s: java LlistrarDirectori <directori>");
		}
	}
}
