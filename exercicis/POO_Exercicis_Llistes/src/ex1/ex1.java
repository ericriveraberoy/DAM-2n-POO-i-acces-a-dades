package ex1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class ex1 {
	/*
	 * Demana cadenes a l'usuari fins que una d'elles sigui buida.
	 * Guarda totes les cadenes que ha introdu�t en una llista.
	 * Despr�s, torna a demanar cadenes i per a cadascuna, fes que el programa digui si la cadena s'ha introdu�t abans o no.
	 */

	public static void main(String[] args) {
		ArrayList<String> cadenas = new ArrayList<String>();
		boolean checkEquals = false;
		Iterator<String> it;
		
		String s = new String("-");
		String s2 = new String();
		Scanner k = new Scanner(System.in);
		
		System.out.println("[Introdueix cadenes]");
		while (!s.isEmpty()) {
			System.out.println("> ");
			s = k.nextLine();
			if (!s.isEmpty()) {
				cadenas.add(s);
			}
		}
	
		s = "-";	
		System.out.println("[Comprova si s'ha introduit abans]");
		while (!s.isEmpty()) {
			System.out.println("> ");
			s = k.nextLine();
			if (!s.isEmpty()) {
				it = cadenas.iterator();
				while (it.hasNext()) {
					s2 = it.next();
					if (s2.equals(s)) {
						checkEquals = true;
					}
				}
				if (checkEquals) {
					System.out.println("Si");
				} else {
					System.out.println("No");
				}
				checkEquals = false;
			}
		}
		
		k.close();
	}

}
