package ex7;

/*
	Exercici 7
	
	La cerca bin�ria o cerca dicot�mica �s un algorisme de cerca.
	Per a realitzar-la, �s necessari comptar amb un vector o llista ordenada.
	Llavors, prenem l'element que es troba a la mitat de la llista, i el comparem amb l'element cercat.
	Si l'element cercat �s menor, es pren l'interval que va des de l'element central al principi.
	En cas contrari, prenem l'interval que va des de l'element central fins al final de la llista.
	Procedim aix�, amb intervals cada vegada menors, fins que arribarem a un interval indivisible.
	Llavors, o l'element no �s a la llista, o l'interval trobat �s el nostre element.
	
	Amb aquests exercicis comprovarem el funcionament de la cerca bin�ria i
	practicarem la implementaci� d'estructures din�miques de dades.
	
	Crea la classe LlistaOrdenada que contingui una List i que permeti introduir i suprimir Integer a la llista,
	de manera que els enters sempre es guardaran de forma ordenada.
	Discuteix els avantatges i inconvenients d'utilitzar les diverses implementacions de List disponibles.
	Utilitza un ArrayList.
	
	Implementa l'algorisme de cerca bin�ria al m�tode cerca de la classe LlistaOrdenada.
	El m�tode ha de retornar la posici� que ocupa l'element cercat dins de la llista, o -1 si l'element no hi �s.
*/

public class Main {

	public static void main(String[] args) {
		LlistaOrdenada ll = new LlistaOrdenada();
		
		ll.afegir(1);		
		ll.afegir(2);		
		ll.afegir(3);		
		ll.afegir(10);
		ll.afegir(9);
		ll.afegir(8);
		ll.afegir(7);
		ll.afegir(6);
		ll.afegir(4);
		ll.afegir(5);
		
		System.out.println(ll.toString());
		System.out.println(ll.cercaBinaria(10));
	}

}
