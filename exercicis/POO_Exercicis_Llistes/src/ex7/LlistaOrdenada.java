package ex7;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class LlistaOrdenada {
	List<Integer> l = new ArrayList<Integer>();
	ListIterator<Integer> it;
	
	@Override
	public String toString() {
		return l.toString();
	}
	
	public void afegir(Integer o) {
		l.add(o);
		l.sort(null);
	}
	
	public void mostrar() {
		it = l.listIterator();
		while (it.hasNext()) {
			System.out.println(it.next());
		}
	}
	
	public Integer cerca (Integer o) {
		it = l.listIterator();
		int res = -1;
		
		while (it.hasNext()) {
			if (it.next() == o) {
				res = it.nextIndex();
			}
		}
		return res;
	}
	
	public Integer cercaBinaria (Integer o) {
		int min = 0;
		int max = l.size()-1;
		int pos = max/2;
		int temp;
		boolean trobat = false;

		while (!trobat && min<=max) {
			System.out.println("Min: " + min + "; Pos: " + pos + "; Max: " + max);
			if (l.get(pos) == o) {
				System.out.println("Trobat a la posicio " + pos);
				trobat = true;
			} else if (l.get(pos) > o) {
				System.out.println("El numero " + l.get(pos) + " es major que " + o);
				max = pos-1;
				pos = (max+min)/2;
			} else {
				System.out.println("El numero " + l.get(pos) + " es menor que " + o);
				min = pos+1;
				pos = (max+min)/2;
			}
		}
		
		if (!trobat) return -1;
		else return pos;
	}
}
