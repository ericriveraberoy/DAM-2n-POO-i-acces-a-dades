package ex3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

public class ex3 {
	/*
	 Donada la seg�ent llista de popularitat dels llenguatges,
	 on el primer �s el m�s popular i l'�ltim el que menys:

	llenguatges = "Java", "C", "C++", "Visual Basic", "PHP", "Python", "Perl", "C#", "JavaScript","Delphi"

	Extreu (no cal que facis un men�, simplement que el programa extregui seq�encialment):
	    El llenguatge m�s popular de la llista.
	    El sis� m�s popular.
	    Els tres m�s populars.
	    Els tres menys populars.
	    Tots menys el primer.
	    Imprimeix la llista amb cada llenguatge en una l�nia i numerada.
	    Nombre total de llenguatges a la llista.

	 */

	public static void main(String[] args) {
		List<String> llenguatges = new ArrayList<String>();
		llenguatges.addAll(Arrays.asList("Java", "C", "C++", "Visual Basic", "PHP", "Python", "Perl", "C#", "JavaScript", "Delphi"));
		ListIterator<String> it;

		System.out.println("Llenguatge m�s popular de la llista: " + llenguatges.get(0));
		System.out.println("Sis� m�s popular: " + llenguatges.get(5));

		System.out.println("Tres mes populars: ");
		it = llenguatges.subList(0, 2).listIterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}

		System.out.println("Tres menys populars: ");
		it = llenguatges.listIterator(llenguatges.size()-3);
		while (it.hasNext()) {
			System.out.println(it.next());
		}

		System.out.println("Tots menys el primer: ");
		it = llenguatges.listIterator(llenguatges.size()-(llenguatges.size()-1));
		while (it.hasNext()) {
			System.out.println(it.next());
		}

		System.out.println("Llista amb cada llenguatge en una l�nia i numerada: ");
		it = llenguatges.listIterator();
		while (it.hasNext()) {
			System.out.println(it.nextIndex()+1 + " - " + it.next());
		}
		
		System.out.println("Nombre total de llenguatges a la llista: " + llenguatges.size());
	}

}
