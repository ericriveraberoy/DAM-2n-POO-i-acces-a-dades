package ex2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ex2 {

	/*
	 * Fes una funci� que rebi com a par�metre una llista de nombres i que retorni true
	 * si la llista est� ordenada de menor a major i false en cas contrari. 	
	 */
	
	public static boolean ordenades(List<Integer> a) {
		boolean c = true;
		Iterator<Integer> it = a.iterator();
		
		int actual;
		int anterior = -999999999;
		
		while (it.hasNext() && c) {
			actual = it.next();
			
			if (actual >= anterior) c = true;
			else c = false;
			
			anterior = actual;
		}
		
		return c;
	}
	
	public static void main(String[] args) {
		List<Integer> a = new ArrayList<Integer>();
		a.add(1);
		a.add(2);
		a.add(3);
		a.add(4);
				
		System.out.println(ordenades(a));
	}

}
