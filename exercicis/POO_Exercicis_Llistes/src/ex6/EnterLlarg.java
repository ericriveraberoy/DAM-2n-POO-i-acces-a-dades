package ex6;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class EnterLlarg {
	List<Integer> array = new ArrayList<Integer>();
	
	public EnterLlarg (long n) {
		String s = Long.toString(n);
		
		for (int i = 0; i < s.length(); i++) {
			array.add(Integer.parseInt(String.valueOf(s.charAt(i))));
		}
	}
	
	public EnterLlarg (byte[] n) {
		for (int i = 0; i < n.length; i++) {
			String s = Byte.toString(n[i]);
			array.add(Integer.parseInt(s));
		}
	}
	
	public EnterLlarg (String s) {
		for (int i = 0; i < s.length(); i++) {
			array.add(Integer.parseInt(String.valueOf(s.charAt(i))));
		}
		
	}
	
	public String toString() {
		Iterator<Integer> it = array.iterator();
		String s = new String();
		
		while(it.hasNext()) {
			s += it.next();
		}
		
		return s;
	}
	
	public EnterLlarg suma (EnterLlarg e2) {
		Long i = Long.parseLong(this.toString());
		Long i2 = Long.parseLong(e2.toString());
		Long suma = i + i2;
		
		return new EnterLlarg(suma.toString());
	}
}
