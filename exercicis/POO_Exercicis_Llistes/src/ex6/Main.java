package ex6;

public class Main {

	/*

	Exercici 6:
		Per a representar un enter llarg, de m�s de 30 xifres, utilitzarem un vector din�mic (ArrayList),
		que contingui en el camp dades de cada node una xifra de l�enter.

		Escriviu una classe EnterLlarg que encapsuli l'ArrayList i que permeti inicialitzar-lo amb un long,
		un vector de byte, o un String amb la representaci� del nombre.

		Escriviu un programa en qu� es llegeixin dos enters llargs de teclat, es cre�n els corresponents EntersLlargs
		i es mostrin per pantalla (recorrent la llista per obtenir de nou les xifres).
		Per aix� caldr� sobreescriure el m�tode toString de la classe EnterLlarg.

		Obteniu i mostreu una nova llista que emmagatzemi la suma dels dos nombres anteriors.
		Per fer aix� caldr� afegir el m�tode suma a la classe EnterLlarg.

		Compara aquesta implementaci� amb la classe BigInteger de Java.
		Creus que internament s'han implementat de forma similar?

	 */
	
	public static void main(String[] args) {
		long n = 1234567894513245679L;
		EnterLlarg e = new EnterLlarg(n);
		System.out.println(e.toString());
		
		byte[] b = {1, 2, 3, 4, 5, 6, 7, 8, 9, 9, 9, 9, 9};
		EnterLlarg e2 = new EnterLlarg(b);
		System.out.println(e2.toString());
		
		EnterLlarg e3 = e2.suma(e);
		System.out.println(e3.toString());
	}

}
