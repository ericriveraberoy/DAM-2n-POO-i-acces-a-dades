package ex5;

import java.util.ArrayList;
import java.util.Iterator;

public class Agenda {
	private ArrayList<Contacte> contactes = new ArrayList<Contacte>();
	
	public void afegeixContacte(String nom, String cognoms, String adreca, String dni, Integer tlf) {
		Iterator<Contacte> it = contactes.iterator();
		boolean check = true;
		
		while(it.hasNext()) {
			Contacte c = it.next();
			if (dni.equals(c.getDni())) check = false;
		}
		if (check) {
			contactes.add(new Contacte(nom, cognoms, adreca, dni, tlf));
		}
	}
	
	public Contacte cercaContacte(String dni) {
		Iterator<Contacte> it = contactes.iterator();
		
		while (it.hasNext()) {
			Contacte c = it.next();
			if (c.getDni().equals(dni)) {
				return c;
			}
		}
		return null;
	}
	
	public void esborraContacte(String dni) {
		Iterator<Contacte> it = contactes.iterator();
		while (it.hasNext()) {
			if (it.next().getDni().equals(dni)) {
				it.remove();
			}
		}
	}
}
