package ex5;

import java.util.Scanner;

public class ex5 {
	/*

	En aquest exercici implementarem una petita agenda.
	Crea la classe Contacte per a guardar la informaci� de cadascun dels contactes,
	i la classe Agenda, que contindr� una llista de contactes.
	
	Fes un programa que a trav�s d'un men� permeti realitzar les operacions habituals sobre una agenda:
	    Afegir elements (s'inseriran mantenint l'ordre alfab�tic per cognom i nom).
	    Esborrar algun element, donat el seu nom.
	    Modificar el nom, cognom, adre�a o tel�fon d'un element.
	    Cercar un element donada alguna de les dades.
	 */

	public static void main(String[] args) {
		Agenda agenda = new Agenda();
		
		int opcio = -1;
		Scanner k = new Scanner(System.in);
		
		while (opcio != 0) {
			System.out.println("\n[ AGENDA ]");
			System.out.println("1. Afegir element");
			System.out.println("2. Esborrar element.");
			System.out.println("3. Modificar element.");
			System.out.println("4. Cercar element.");
			System.out.println("0. Surt.");
			System.out.println("> ");
			opcio = k.nextInt();
			
			switch(opcio) {
			case 1:
				afegirElement(agenda);
				break;
			case 2:
				esborrarElement(agenda);
				break;
			case 3:
				modificarElement(agenda);
				break;
			case 4:
				cercarElement(agenda);
				break;
			case 0:
				break;
			default:
				System.out.println("Opci� no reconeguda!");
			}
		}
	}
	
	public static void afegirElement(Agenda agenda) {
		String nom = new String();
		String cognoms = new String();
		String adreca = new String();
		String dni = new String();
		Integer tlf = new Integer(0);
		
		Scanner k = new Scanner(System.in);
		
		System.out.println("Nom: ");
		nom = k.nextLine();
		
		System.out.println("Cognoms: ");
		cognoms = k.nextLine();
		
		System.out.println("Adre�a: ");
		adreca = k.nextLine();
		
		System.out.println("DNI: ");
		dni = k.nextLine();
		
		System.out.println("Tel�fon: ");
		tlf = k.nextInt();
		
		agenda.afegeixContacte(nom, cognoms, adreca, dni, tlf);
	}
	
	public static void esborrarElement(Agenda agenda) {
		String dni = new String();
		Scanner k = new Scanner(System.in);
		
		System.out.println("Escriu el DNI del contacte a esborrar: ");
		dni = k.nextLine();
		
		Contacte c = agenda.cercaContacte(dni);
		if (c != null) {
			System.out.println("Vols esborrar a " + c.getNom() + " " + c.getCognoms() + " amb DNI " + c.getDni() + "? (S/n)");
			String resposta = k.nextLine();
			
			if (!resposta.equals("n")) {
				agenda.esborraContacte(dni);
			}
		} else {
			System.out.println("No s'ha trobat el contacte!");
		}
	}

	public static void modificarElement(Agenda agenda) {
		String dni = new String();
		Scanner k = new Scanner(System.in);
		
		System.out.println("Escriu el DNI del contacte a modificar: ");
		dni = k.nextLine();
		
		Contacte c = agenda.cercaContacte(dni);
		if (c != null) {
			System.out.println("Nom (" + c.getNom() + "): ");
			c.setNom(k.nextLine());
			
			System.out.println("Cognoms (" + c.getCognoms() + "): ");
			c.setCognoms(k.nextLine());
			
			System.out.println("Adre�a (" + c.getAdreca() + "): ");
			c.setAdreca(k.nextLine());
			
			System.out.println("DNI (" + c.getDni() + "): ");
			c.setDni(k.nextLine());
			
			System.out.println("Tel�fon (" + c.getTlf() + "): ");
			c.setTlf(k.nextInt());
		} else {
			System.out.println("No s'ha trobat el contacte!");
		}
	}
	
	public static void cercarElement(Agenda agenda) {
		String dni = new String();
		Scanner k = new Scanner(System.in);
		
		System.out.println("Escriu el DNI del contacte a cercar: ");
		dni = k.nextLine();
		
		Contacte c = agenda.cercaContacte(dni);
		if (c != null) {
			System.out.println("\nNom: " + c.getNom());			
			System.out.println("Cognoms: " + c.getCognoms());			
			System.out.println("Adre�a: " + c.getAdreca());			
			System.out.println("DNI: " + c.getDni());			
			System.out.println("Tel�fon: " + c.getTlf());
		} else {
			System.out.println("No s'ha trobat el contacte!");
		}
		
		System.out.println("\nPresiona una tecla per continuar...");
		k.nextLine();
	}
}
