package ex5;

public class Contacte {
	private String nom;
	private String cognoms;
	private String adreca;
	private String dni;
	private Integer tlf;
	
	public Contacte(String nom, String cognoms, String adreca, String dni, Integer tlf) {
		this.nom = nom;
		this.cognoms = cognoms;
		this.adreca = adreca;
		this.dni = dni;
		this.tlf = tlf;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCognoms() {
		return cognoms;
	}

	public void setCognoms(String cognoms) {
		this.cognoms = cognoms;
	}

	public String getAdreca() {
		return adreca;
	}

	public void setAdreca(String adreca) {
		this.adreca = adreca;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public Integer getTlf() {
		return tlf;
	}

	public void setTlf(Integer tlf) {
		this.tlf = tlf;
	}
}
