package exercicis;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Main {
	/*

		Exercici 2. Detector de text.
			Fes un programa que rebi per par�metre un o m�s noms de fitxer i que,
			per a cadascun d'ells, mostri per pantalla nom�s aquelles parts que s�n text,
			saltant-se les parts bin�ries.

			Utilitza m�todes de Character per a la detecci� senzilla de qu� �s text i qu� no.

			Un error en un fitxer no ha d'interrompre el programa, que haur� de saltar al fitxer seg�ent.

	 */

	public static void main(String[] args) {
		for (String arg : args) {
			System.out.println(arg);
			try (DataInputStream w = new DataInputStream(new FileInputStream(arg))) {
				int a = 0;
				while (a != -1) {
					a = w.read();
					if (a != -1) {
						char[] c = Character.toChars(a);
						System.out.print(c);
					}
				}
				System.out.println();
			} catch (FileNotFoundException ex) {
				System.err.println(arg + ": No s'ha trobat el fitxer");
			} catch (IOException ex) {
				System.err.println(ex);
			}
		}
	}

}
