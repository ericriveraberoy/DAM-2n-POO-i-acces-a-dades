package exercici;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {
	/*
		Exercici 3. Versi� simple del cp:
		
			Fes un programa que rebi per par�metres el nom d'un fitxer i el
			nom d'un directori i que copi� el fitxer dins del directori.
			
			Pista: utilitza el m�tode Files.copy.
	 */

	public static void main(String[] args) {
		if (Files.isDirectory(Paths.get(args[1]))) {
			String fitxer = args[0].toString();
			String directori = args[1].toString();
			Path origen = Paths.get(directori + "\\" + fitxer);
			Path desti = Paths.get(directori + "\\copy-" + fitxer);
			try {	
				Files.copy(origen, desti);
				System.out.println("[OK] Fitxer copiat correctament.");
			} catch (FileAlreadyExistsException e) {
				System.out.println("[Error] El fitxer de dest� ja existeix.");
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("\"" + args[1] + "\": Directori invalid.");
		}
	}

}
