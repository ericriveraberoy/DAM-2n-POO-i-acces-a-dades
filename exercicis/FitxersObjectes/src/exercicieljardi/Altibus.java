package exercicieljardi;

import java.io.Serializable;

public class Altibus extends Planta implements Serializable {
	
	public Llavor creix() {
		// Creix com qualsevol Planta
		super.creix();
		
		// A m�s, si la seva altura es superior a 7 deixar� anar una Llavor que,
		//  quan germini, donar� lloc a una nova Altibus
		if (altura > 7) {
			Altibus altibus = new Altibus();
			Llavor llavor = new Llavor(altibus);
			return llavor;
		} else {
			return null;
		}
	}

	@Override
	public char getChar(int nivell) {
		if (nivell == altura) {
			return "O".charAt(0);
		} else if (nivell < altura) {
			return "|".charAt(0);
		} else return " ".charAt(0);
	}

}
