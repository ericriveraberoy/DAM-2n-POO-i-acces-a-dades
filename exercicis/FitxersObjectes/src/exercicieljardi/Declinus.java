package exercicieljardi;

import java.io.Serializable;

public class Declinus extends Planta implements Serializable {
	private boolean potCreixer = false;
	private boolean asc = true;
	private boolean retornaLlavor = false;
	
	public Llavor creix() {
		// Primer de tot comprovem si pot creixer:
		if (potCreixer == true) { // Si pot creixer...
			// Si pot creixer, comprovem si creix o decreix:
			if (asc == true) { // Si creix...
				altura++; // Sumem +1 a la seva altura.
				if (altura == 4) { // A m�s, si la seva altura es 4, canviem a decreix i deixem anar una llavor.
					asc = false;
					retornaLlavor = true;
				} else return null;
			} else { // Si decreix...
				if (altura == 4) { // Si comen�a ara a decreixer, deixem anar una llavor.
					retornaLlavor = true;
				}
				altura--;
				if (altura == 0) { // Si la seva altura es 0, mor.
					esViva = false;
				}
			}
		} else { // Si no pot creixer, no fem res
			potCreixer = true;
		}
		
		// Comprovem si aquesta vegada li toca deixar anar una llavor
		if (retornaLlavor == true) {
			retornaLlavor = false;
			Declinus d = new Declinus();
			Llavor ll = new Llavor(d);
			return ll;
		} else return null;
	}

	@Override
	public char getChar(int nivell) {
		if (nivell == altura) {
			return "*".charAt(0);
		} else if (nivell < altura) {
			return ":".charAt(0);
		} else return " ".charAt(0);
	}

}
