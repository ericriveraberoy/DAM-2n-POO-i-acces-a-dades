package exercicieljardi;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Scanner;

public class Principal implements Serializable {

	static int MIDA_JARDI = 20;
	
	public static void main(String[] args) {
		
		Jardi jardi = null;
		
		/*
		 * Comprovo si hi ha una partida guardada
		 */
		try (ObjectInputStream lector = new ObjectInputStream(new FileInputStream("jardi.sav"));) {
			while (true) {
                Object o = lector.readObject();
                if (o instanceof Jardi) {
                    jardi = (Jardi)o;
                }
			}
        } catch (EOFException ex) {
            System.out.println("S'ha carregat l'ultima partida guardada.");
        } catch (ClassNotFoundException ex) {
            System.err.println(ex);
        } catch (FileNotFoundException ex) {
        	System.out.println("No hi ha cap partida guardada, comen�ant una nova...");
        	// Creo un jardi i un parell tipus de plantes
    		jardi = new Jardi(MIDA_JARDI);
    		Altibus a = new Altibus();
    		Declinus d = new Declinus();
    		
    		// Planto llavors dels tipus de plantes al meu jardi
    		jardi.plantaLlavor(a, 2);
    		jardi.plantaLlavor(d, 7);
    		jardi.plantaLlavor(a, 12);
    		jardi.plantaLlavor(d, 16);
    		jardi.plantaLlavor(a, 19);
        } catch (IOException ex) {
            System.err.println(ex);
        }
		
		/*
		 * Un cop cargada/creada una partida, comen�o:
		 */
		
		// Faig passar el temps fins que l'usuari escriu "surt"
		String s = new String();
		Scanner sc = new Scanner (System.in);        
		do {
			// Faig passar el temps i dibuixo el jardi
			jardi.temps();
			System.out.print(jardi.toString());
			
			// [Opcional] Per cada vegada que dibuixi un jardi, possa-li un "terra"
			for (int i = 0; i < MIDA_JARDI; i++) System.out.print("_");

			// Demano a l'usuari que escrigui
			s = sc.nextLine ();
			if (s.equals("surt")) {
				guardaifinalitza(jardi);
			}
		} while (!s.equals("surt"));
		sc.close();
	}

	public static void guardaifinalitza(Jardi jardi) {
		try (ObjectOutputStream escriptor = new ObjectOutputStream(new FileOutputStream("jardi.sav"))) {
			escriptor.writeObject(jardi);
        } catch (IOException ex) {
            System.err.println(ex);
        }
	}
	
}
