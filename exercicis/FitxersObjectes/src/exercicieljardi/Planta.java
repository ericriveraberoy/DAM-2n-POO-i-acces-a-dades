package exercicieljardi;

import java.io.Serializable;
import java.util.Random;

public abstract class Planta implements Serializable {
	protected boolean esViva = true;
	protected int altura = 0;
	
	public abstract char getChar(int nivell);
	
	public Planta creix() {
		if (this.altura < 10) {
			altura++;
		} else {
			esViva = false;
		}
		return null;
	};
	
	public int escampaLlavor() {
		// Generem un numero random entre -2 i 1
		Random r = new Random();
		int r1 = r.nextInt(4) - 2;
		
		// I, si es 0, el convertim a 2
		if (r1 == 0) {
			r1 = 2;
		}
		return r1;
	}
	
	public int getAltura() {
		return altura;
	}
	
	public boolean esViva() {
		return esViva;
	}

}
