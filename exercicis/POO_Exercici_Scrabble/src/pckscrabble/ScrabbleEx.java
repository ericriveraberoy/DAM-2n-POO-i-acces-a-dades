package pckscrabble;

public class ScrabbleEx implements Comparable<ScrabbleEx> {

	Casella c[];

	public ScrabbleEx(Casella[] c) {
		this.c = c;
	}
	
	public int valor() {
		int v = 0;
		int m = 1;
		for (int i=0; i<this.c.length; i++) {
			m *= c[i].getMultiplicadorParaula();
			switch (this.c[i].getLletra()) {
				case 'A':
				case 'E':
				case 'I':
				case 'L':
				case 'N':
				case 'O':
				case 'R':
				case 'S':
				case 'T':
				case 'U':
					v += 1*c[i].getMultiplicadorCasella();
					break;
				case 'D':
				case 'G':
					v += 2*c[i].getMultiplicadorCasella();
					break;
				case 'B':
				case 'C':
				case 'M':
				case 'P':
					v += 3*c[i].getMultiplicadorCasella();
					break;
				case 'F':
				case 'H':
				case 'V':
				case 'W':
				case 'Y':
					v += 4*c[i].getMultiplicadorCasella();
					break;
				case 'K':
					v += 5*c[i].getMultiplicadorCasella();
					break;
				case 'J':
				case 'X':
					v += 8*c[i].getMultiplicadorCasella();
					break;
				case 'Q':
				case 'Z':
					v += 10*c[i].getMultiplicadorCasella();
					break;
			}
		}
		return v*m;
	}

	@Override
	public int compareTo(ScrabbleEx s2) {
		if (this.valor() > s2.valor()) return 1;
		else if (this.valor() < s2.valor()) return -1;
		else return 0;
	}
	
}
