package pckscrabble;

import java.util.Comparator;

public class ComparadorScrabble implements Comparator<String> {
	
	private static ComparadorScrabble COMPARADOR_SCRABBLE = null;

	private ComparadorScrabble() {
		
	}
	
	public int valor (String paraula) {
		int v = 0;
		for (int i=0; i<paraula.length(); i++) {
			switch (paraula.charAt(i)) {
				case 'A':
				case 'E':
				case 'I':
				case 'L':
				case 'N':
				case 'O':
				case 'R':
				case 'S':
				case 'T':
				case 'U':
					v += 1;
					break;
				case 'D':
				case 'G':
					v += 2;
					break;
				case 'B':
				case 'C':
				case 'M':
				case 'P':
					v += 3;
					break;
				case 'F':
				case 'H':
				case 'V':
				case 'W':
				case 'Y':
					v += 4;
					break;
				case 'K':
					v += 5;
					break;
				case 'J':
				case 'X':
					v += 8;
					break;
				case 'Q':
				case 'Z':
					v += 10;
					break;					
			}
		}
		return v;
	}
	
	@Override
	public int compare(String s1, String s2) {
		if (valor(s1) > valor(s2))
			return 1;
		else if (valor(s1) < valor(s2))
			return -1;
		else
			return 0;
	}
	
	public static ComparadorScrabble getInstance() {
		if (COMPARADOR_SCRABBLE == null) {
			COMPARADOR_SCRABBLE = new ComparadorScrabble();
		}
		return COMPARADOR_SCRABBLE;
	}
}
