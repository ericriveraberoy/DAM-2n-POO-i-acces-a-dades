package pckscrabble;

public class Casella implements Cloneable {
	private char lletra;
	private int multiplicadorCasella;
	private int multiplicadorParaula;
	
	/* Constructors */
	public Casella(char lletra, int multiplicadorCasella, int multiplicadorParaula) {
		this.lletra = lletra;
		this.multiplicadorCasella = multiplicadorCasella;
		this.multiplicadorParaula = multiplicadorParaula;
	}
	
	public Casella(char lletra) {
		this(lletra, 1, 1);
	}
	
	/* Getters i Setters */
	public char getLletra() {
		return lletra;
	}
	
	public void setLletra(char lletra) {
		this.lletra = lletra;
	}
	
	public int getMultiplicadorCasella() {
		return multiplicadorCasella;
	}
	
	public void setMultiplicadorCasella(int multiplicadorCasella) {
		this.multiplicadorCasella = multiplicadorCasella;
	}
	
	public int getMultiplicadorParaula() {
		return multiplicadorParaula;
	}
	
	public void setMultiplicadorParaula(int multiplicadorParaula) {
		this.multiplicadorParaula = multiplicadorParaula;
	}
	
	
	/* Cloneable */
	public Casella clone () {
		Casella c = new Casella(this.lletra, this.multiplicadorCasella, this.multiplicadorParaula);
		return c;
	}
}
