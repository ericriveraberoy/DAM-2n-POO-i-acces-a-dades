package pckscrabble;

import java.util.Arrays;

public class Main {

	public static void main(String[] args) {
		
		/* Crea un programa que cre� un vector d'objectes Scrabble i un vector de String,
		   i que s'ordenin segons la seva puntuaci� al joc. */
		Scrabble s1[] = {
				new Scrabble("RIPOLLET"),
				new Scrabble("CERDANYOLA"),
				new Scrabble("SABADELL"),
				new Scrabble("BARCELONA"),
				new Scrabble("MONTCADA")
		};
		
		String s2[] = {
				"RIPOLLET",
				"CERDANYOLA",
				"SABADELL",
				"BARCELONA",
				"MONTCADA"
		};
		
		
		Arrays.sort(s1);
		for (int i = 0; i < s1.length; i++) {
			System.out.println(s1[i].paraula + " - " + s1[i].valor());
		}
		
		System.out.println();
		
		Arrays.sort(s2, ComparadorScrabble.getInstance());
		for (int i = 0; i < s2.length; i++) {
			System.out.println(s2[i]);
		}
		
		System.out.println();
		
		/* Crea un vector de caselles, i a partir d'ell un objecte ScrabbleEx i mostra el seu valor. */
		Casella c[] = {
				new Casella('M'),
				new Casella('O'),
				new Casella('N'),
				new Casella('T'),
				new Casella('C'),
				new Casella('A'),
				new Casella('D'),
				new Casella('A'),
		};
		
		ScrabbleEx s3 = new ScrabbleEx(c);
		System.out.println(s3.valor());
	}
}
