package pckscrabble;

public class Scrabble implements Comparable <Scrabble> {
	String paraula;
	
	public Scrabble(String s) {
		// Comprovem si la paraula cont� car�cters no permesos
		for (int i=0; i<s.length(); i++) {
			if (!Character.isLetter(s.charAt(i))) {
				throw new IllegalArgumentException();
			}
		}
		this.paraula = s;
	}
	
	public int valor() {
		int v = 0;
		for (int i=0; i<this.paraula.length(); i++) {
			switch (this.paraula.charAt(i)) {
				case 'A':
				case 'E':
				case 'I':
				case 'L':
				case 'N':
				case 'O':
				case 'R':
				case 'S':
				case 'T':
				case 'U':
					v += 1;
					break;
				case 'D':
				case 'G':
					v += 2;
					break;
				case 'B':
				case 'C':
				case 'M':
				case 'P':
					v += 3;
					break;
				case 'F':
				case 'H':
				case 'V':
				case 'W':
				case 'Y':
					v += 4;
					break;
				case 'K':
					v += 5;
					break;
				case 'J':
				case 'X':
					v += 8;
					break;
				case 'Q':
				case 'Z':
					v += 10;
					break;					
			}
		}
		return v;
	}

	@Override
	public int compareTo(Scrabble s2) {
		if (this.valor() > s2.valor()) return 1;
		else if (this.valor() < s2.valor()) return -1;
		else return 0;
	}

}
