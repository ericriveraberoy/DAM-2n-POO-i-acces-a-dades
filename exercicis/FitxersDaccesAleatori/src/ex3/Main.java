package ex3;

import java.io.IOException;
import java.io.RandomAccessFile;

/*
	Aquest programa utilitza el fitxer generat a l'exercici genera secret.

	Es demanar� un codi a l'usuari i es cercar� al fitxer.
	Per fer-ho, s'utilitzar� una cerca bin�ria: es comen�ar� pel codi que hi ha a la mitat del fitxer,
	es mirar� si �s major o menor que el codi demanat, i es procedir� amb la mitat corresponent del fitxer.
	Utilitza un fitxer d'acc�s aleatori per poder-ho fer.

	El programa mostrar� el secret que correspon al codi introdu�t, si hi �s, o indicar� que no hi �s en cas contrari.
*/

public class Main {

	public static void main(String[] args) {

		try (RandomAccessFile fitxer = new RandomAccessFile("C:\\Users\\ies\\Desktop\\prova.bin", "rw")) {
			
			int id = 512;
			
			long min = 0;
			long max = fitxer.length();
			long pos = max/2;
			boolean trobat = false;
			
			while (!trobat && min<=max) {
				fitxer.seek(pos);
				int numero = fitxer.readInt();
				if (numero == id) {
					System.out.println("Trobat a la posicio " + pos);
					trobat = true;
				} else if (numero > id) {
					System.out.println("El numero " + numero + " es major que " + id);
					max = pos-1;
					pos = (max+min)/2;
				} else {
					System.out.println("El numero " + numero + " es menor que " + id);
					min = pos+1;
					pos = (max+min)/2;
				}
			}
			
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		
	}

}
