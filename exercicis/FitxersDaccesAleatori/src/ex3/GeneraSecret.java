package ex3;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

public class GeneraSecret {

	/*
		En aquest programa, es generar� un fitxer binari que contindr� una s�rie de parelles codi-secret.
		El codi �s un nombre enter i el secret una cadena de 3 car�cters.
		El fitxer estar� ordenat de menor a major pels codis.
		
		Per generar el fitxer es procedir� de la seg�ent manera:
		el primer codi ser� un nombre aleatori entre 1 i 3.
		Cadascun dels seg�ents codis ser� com l'anterior sumant-hi una quantitat entre 1 i 3.
		Els secrets seran combinacions de tres lletres agafades aleat�riament d'entre el conjunt de lletres min�scules.
		
		El programa generar� un total de 1000 parelles codi-secret.
		
		Nota: per guardar les cadenes utilitza writeChars i no writeUTF, ja que la longitud d'una cadena UTF �s variable.
	 */

	public static void main(String[] args) {
		try (DataOutputStream escriptor = new DataOutputStream(new FileOutputStream("C:\\Users\\ies\\Desktop\\prova.bin"))) {
			Random r = new Random();
			String secret = new String();
			int codi = r.nextInt(3);
			
			for (int i = 0; i < 1000; i++) {
				codi = codi + r.nextInt(2)+1;
				secret = generateString(r, "abcdefghijklmnopqrstuvwxyz", 3);
				
				escriptor.writeInt(codi);
				escriptor.writeChars(secret);
				System.out.println(codi + " - " + secret);
			}
        } catch (IOException e) {
        	System.err.println(e);
        }
    }
	
	public static String generateString(Random rng, String characters, int length) {
	    char[] text = new char[length];
	    for (int i = 0; i < length; i++) {
	        text[i] = characters.charAt(rng.nextInt(characters.length()));
	    }
	    return new String(text);
	}
}
