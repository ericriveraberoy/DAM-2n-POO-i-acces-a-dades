package ex2;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class Main {
	
	private static String readChars(RandomAccessFile fitxer, int nChars) throws IOException {
		StringBuilder b = new StringBuilder();
		char ch = ' ';
		for (int i=0; i<nChars; i++) {
			ch = fitxer.readChar();
			if (ch != '\0')
				b.append(ch);
		}
		return b.toString();
	}

	public static void main(String[] args) {
		int id;
		long pos=0;
		Scanner scanner = new Scanner(System.in);

		System.out.println("Introdueix n�mero de registre: ");
		id=scanner.nextInt();
		scanner.nextLine();
		try (RandomAccessFile fitxer = new RandomAccessFile("C:\\Users\\ies\\Desktop\\paisos.dat", "rw")) {
			// per accedir a un registre multipliquem per la mida de cada registre.
			pos=(id-1)*174;

			if (pos<0 || pos>=fitxer.length())
				throw new IOException("N�mero de registre inv�lid.");

			fitxer.seek(pos);
			fitxer.readInt(); // Saltem l'id
			System.out.println("Pais: " + readChars(fitxer, 40) +
								"\nISO: " + readChars(fitxer, 3) +
								"\nCapital: " + readChars(fitxer, 40) +
								"\nPoblacio: " + fitxer.readInt());
			
			
			System.out.println("Nou nom Pais: ");
			String pais = new String(scanner.nextLine());
			
			System.out.println("Nova ISO: ");
			String iso = new String(scanner.nextLine());
			
			System.out.println("Nova Capital: ");
			String capital = new String(scanner.nextLine());
			
			System.out.println("Nova Poblacio: ");
			Integer poblacio = scanner.nextInt();
			scanner.nextLine();
			
			Pais p = new Pais(pais, iso, capital);
			p.setPoblacio(poblacio);
			
			fitxer.seek(pos+4);
			
			StringBuilder b = null;
			
			b = new StringBuilder(p.getNom());
			b.setLength(40);
			fitxer.writeChars(b.toString());
			
			b = new StringBuilder(p.getCodiISO());
			b.setLength(3);
			fitxer.writeChars(b.toString());
			
			b = new StringBuilder(p.getCapital());
			b.setLength(40);
			fitxer.writeChars(b.toString());
			
			fitxer.writeInt(p.getPoblacio());
			
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		scanner.close();
	}

}
