package ex1;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class ModificarPaisos {

	private static String readChars(RandomAccessFile fitxer, int nChars) throws IOException {
		StringBuilder b = new StringBuilder();
		char ch = ' ';
		for (int i=0; i<nChars; i++) {
			ch = fitxer.readChar();
			if (ch != '\0')
				b.append(ch);
		}
		return b.toString();
	}
	
	public static void main(String[] args) {
		int id;
		long pos=0;
		Scanner scanner = new Scanner(System.in);

		System.out.println("Introdueix n�mero de registre: ");
		id=scanner.nextInt();
		scanner.nextLine();
		try (RandomAccessFile fitxer = new RandomAccessFile("C:\\Users\\ies\\Desktop\\paisos.dat", "rw")) {
			// per accedir a un registre multipliquem per la mida de cada registre.
			pos=(id-1)*174;

			if (pos<0 || pos>=fitxer.length())
				throw new IOException("N�mero de registre inv�lid.");

			fitxer.seek(pos);
			fitxer.readInt(); // Saltem l'id
			System.out.println("Pais: " + readChars(fitxer, 40) +
								"\nISO: " + readChars(fitxer, 3) +
								"\nCapital: " + readChars(fitxer, 40) +
								"\nPoblacio: " + fitxer.readInt());
			
			String opcio = new String("-");
			String s = new String("");
			StringBuilder b = null;
			while (!opcio.equals("Sortir")) {
				System.out.println("Quin camp vols modificar? (Pais, ISO, Capital, Poblacio; Sortir)");
				opcio = scanner.nextLine();
				switch (opcio) {
					case "Pais":
						fitxer.seek(pos+4);
						System.out.println("Nou nom de Pais: ");
						s = scanner.nextLine();
						b = new StringBuilder(s);
						b.setLength(40);
						fitxer.writeChars(b.toString());
						break;
					case "ISO":
						fitxer.seek(pos+84);
						System.out.println("Nova ISO: ");
						s = scanner.nextLine();
						b = new StringBuilder(s);
						b.setLength(3);
						fitxer.writeChars(b.toString());
						break;
					case "Capital":
						fitxer.seek(pos+90);
						System.out.println("Nova capital: ");
						s = scanner.nextLine();
						b = new StringBuilder(s);
						b.setLength(40);
						fitxer.writeChars(b.toString());
						break;
					case "Poblacio":
						fitxer.seek(pos+170);
						System.out.println("Nova poblacio: ");
						int i = scanner.nextInt();
						scanner.nextLine();
						fitxer.writeInt(i);
						break;
					case "Sortir":
						break;
					default:
						System.out.println("Opcio no reconeguda! (Pais, ISO, Capital, Poblacio; Sortir)");
						break;
				}
			}
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		scanner.close();
	}

}
