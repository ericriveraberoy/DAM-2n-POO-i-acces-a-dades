package ex1;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class LectorRegistre {
	private static String readChars(RandomAccessFile fitxer, int nChars) throws IOException {
		StringBuilder b = new StringBuilder();
		char ch = ' ';
		for (int i=0; i<nChars; i++) {
			ch=fitxer.readChar();
			if (ch != '\0')
				b.append(ch);
		}
		return b.toString();
	}

	public static void main(String[] args) {
		String nom;
		int id, poblacio;
		long pos=0;
		Scanner scanner = new Scanner(System.in);

		System.out.println("Introdueix n�mero de registre: ");
		id=scanner.nextInt();
		scanner.nextLine();
		try (RandomAccessFile fitxer = new RandomAccessFile("C:\\Users\\ies\\Desktop\\paisos.dat", "rw")) {
			// per accedir a un registre multipliquem per la mida de
			// cada registre.
			pos=(id-1)*174;

			if (pos<0 || pos>=fitxer.length())
				throw new IOException("N�mero de registre inv�lid.");

			fitxer.seek(pos);
			fitxer.readInt(); // Saltem l'id
			nom = readChars(fitxer, 40);
			readChars(fitxer, 43); // Ens saltem el codi ISO i la capital
			poblacio = fitxer.readInt();
			System.out.println("Pa�s: "+nom+", poblaci� actual: "+poblacio);
			System.out.println("Introdueix la nova poblaci�: ");
			poblacio = scanner.nextInt();
			scanner.nextLine();
			if (poblacio >= 0) {
				pos = fitxer.getFilePointer()-4; // tornem enrere per sobreescriure la poblaci�
				fitxer.seek(pos);
				fitxer.writeInt(poblacio);
			} else {
				System.err.println("La poblaci� ha de ser positiva.");
			}
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		scanner.close();
	}
}