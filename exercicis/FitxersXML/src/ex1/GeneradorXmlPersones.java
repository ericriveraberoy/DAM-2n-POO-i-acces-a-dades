package ex1;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

public class GeneradorXmlPersones {

	public static void main(String[] args) {
		List<Persona> persones = new ArrayList<Persona>();
		
		// Importem les persones a un ArrayList de persones
		try (ObjectInputStream lector = new ObjectInputStream(new FileInputStream("C:\\Users\\ies\\Desktop\\prova.bin"));) {
			while (true) {
				Object o = lector.readObject();
				if (o instanceof Persona) {
					persones.add((Persona) o);
				}
			}
			// Quan hem importat tots, mostra-ho
		} catch (EOFException ex) {
			try (FileWriter writer = new FileWriter("C:\\Users\\ies\\Desktop\\persones.xml")) {
				/*
				 * Inicialitzo el document XML
				 */
				DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				Document document = builder.newDocument();
				document.setXmlVersion("1.0");
				
				/*
				 * Creo l'element root: persones
				 */
				Element elementPersones = document.createElement("persones");
				document.appendChild(elementPersones);
				
				/*
				 * Per cada persona fare un element
				 */
				for (Persona p : persones) {
					// Inicialitzo l'element persona
					Element elementPersona = document.createElement("persona");

					// Fico el nom
					Element elementNom = document.createElement("nom");
					Text text = document.createTextNode(p.getPersona());
					elementNom.appendChild(text);
					elementPersona.appendChild(elementNom);

					// Fico l'edat
					Element elementEdat = document.createElement("edat");
					Text text2 = document.createTextNode(Integer.toString(p.getEdat()));
					elementEdat.appendChild(text2);
					elementPersona.appendChild(elementEdat);
					
					// Afegeixo la persona a l'arrel
					elementPersones.appendChild(elementPersona);
				}
				
				Source source = new DOMSource(document);
				Result result = new StreamResult(writer);

				Transformer transformer = TransformerFactory.newInstance().newTransformer();
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				transformer.setOutputProperty("{http://xml.apache.org/xalan}indent-amount", "5");
				transformer.transform(source, result);

				Result console = new StreamResult(System.out);
				transformer.transform(source, console);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TransformerConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TransformerFactoryConfigurationError e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TransformerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (IOException ex) {
			System.err.println(ex);
		} catch (ClassNotFoundException ex) {
			System.err.println(ex);
		}
		
	}
}
