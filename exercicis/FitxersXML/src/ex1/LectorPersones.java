package ex1;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

public class LectorPersones {

	public static void main(String[] args) {
		List<Persona> persones = new ArrayList<Persona>();
		
		// Importem les persones a un ArrayList de persones
		try (ObjectInputStream lector = new ObjectInputStream(new FileInputStream("C:\\Users\\ies\\Desktop\\prova.bin"));) {
			while (true) {
				Object o = lector.readObject();
				if (o instanceof Persona) {
					persones.add((Persona) o);
				}
			}
			// Quan hem importat tots, mostra-ho
		} catch (EOFException ex) {
			//System.out.println("Hem arribat a final de fitxer.");
			for (Persona p : persones) {
				System.out.println(p);
			}
		} catch (IOException ex) {
			System.err.println(ex);
		} catch (ClassNotFoundException ex) {
			System.err.println(ex);
		}

	}

}
