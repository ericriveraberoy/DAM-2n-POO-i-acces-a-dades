package ex1;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class EscriptorPersones {

	public static void main(String[] args) {
		Persona[] persones = new Persona[5];
		persones[0] = new Persona("Pepe Duro", 18);
		persones[1] = new Persona("Juan Blando", 22);
		persones[2] = new Persona("Laura Rodriguez", 20);
		persones[3] = new Persona("Iker Jimenez", 35);
		persones[4] = new Persona("Eric Rivera", 21);

		try (ObjectOutputStream escriptor = new ObjectOutputStream(new FileOutputStream("C:\\Users\\ies\\Desktop\\prova.bin"));) {
			for (Persona p : persones) {
				escriptor.writeObject(p);
			}
		} catch (IOException ex) {
			System.err.println(ex);
		}
	}
}
