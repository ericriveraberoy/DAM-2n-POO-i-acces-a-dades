package ex1;

import java.io.IOException;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public class LectorXmlPersonesSAX {
	public static void main(String[] args) {
		try {
			XMLReader processadorXML = XMLReaderFactory.createXMLReader();
			processadorXML.setContentHandler(new ControladorXmlPersonesSAX());
			processadorXML.parse(new InputSource("C:\\Users\\ies\\Desktop\\persones.xml"));
		} catch (SAXException | IOException e) {
			System.err.println(e.getMessage());
		}
	}
}
