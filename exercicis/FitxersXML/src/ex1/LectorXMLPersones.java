package ex1;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;

public class LectorXMLPersones {

	private List<Persona> persones = new ArrayList<Persona>();
	
	public static void main(String[] args) {
		try {
			LectorXMLPersones lector = new LectorXMLPersones();
			lector.llegeixPersones();
			lector.mostraPersones();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}

	public void llegeixPersones() throws IOException {
		try (FileInputStream reader = new FileInputStream("C:\\Users\\ies\\Desktop\\persones.xml")) {
			
			DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document document = documentBuilder.parse(reader);

			// document.getDocumentElement().getNodeName()) �s "mascotes"
			NodeList personesNodeList = document.getElementsByTagName("persona");
			
			for (int i = 0 ; i < personesNodeList.getLength(); i++) { // NodeList no implementa Iterable
				Node personaNode = personesNodeList.item(i);

				if (personaNode.getNodeType() == Node.ELEMENT_NODE) {
					Element personaElement = (Element) personaNode;
					
					persones.add(new Persona(
							getNodeValue("nom", personaElement),
							Integer.parseInt(getNodeValue("edat", personaElement))
					));
				}
			}	
		} catch (IOException | ParserConfigurationException | SAXException e) {
			throw new IOException(e);
		}
	}

	private static String getNodeValue(String etiqueta, Element element) {
		NodeList nodeList = element.getElementsByTagName(etiqueta).item(0).getChildNodes();
		Node node = nodeList.item(0);
		return node.getNodeValue();
	}
	
	public void mostraPersones() {
		for (Persona p : persones)
			System.out.println(p);
	}
	
}
