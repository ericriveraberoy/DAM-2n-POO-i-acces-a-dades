package ex1;

import java.io.Serializable;

public class Persona implements Serializable {
	private static final long serialVersionUID = 1L;
	private String persona;
	private Integer edat;
	
	public Persona(String persona, Integer edat) {
		super();
		this.persona = persona;
		this.edat = edat;
	}

	public String getPersona() {
		return persona;
	}

	public void setPersona(String persona) {
		this.persona = persona;
	}

	public Integer getEdat() {
		return edat;
	}

	public void setEdat(Integer edat) {
		this.edat = edat;
	}
	
	@Override
	public String toString() {
		return "[Nom: " + getPersona() + ", Edat: " + getEdat() + "]";
	}

}
