package Exercici1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Main {

	private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement insereix = null;
    private Scanner sc = new Scanner(System.in);
    private static String PATH_FITXER = "C:\\Users\\ies\\Desktop\\noms.csv";
	
	public static void main(String[] args) throws SQLException {
		Main ex = new Main();
        ex.run();
	}

	private void run() throws SQLException {

		int op = -1;
		while (op != 0) {
			System.out.println(
					  "1- Insereix amb Statement.\n"
					+ "2- Insereix amb PreparedStatement.\n"
					+ "3- Insereix amb una �nica consulta SQL.\n"
					+ "4- Esborra el contingut de la taula.\n"
					+ "0. Surt.");
			System.out.println("  > ");
			op = sc.nextInt();
			
			switch (op) {
				case 1:
					insereixStatement();
					break;
				case 2:
					insereixPreparedStatement();
					break;
				case 3:
					insereixConsultaSQL();
					break;
				case 4:
					esborraTot();
					break;
				case 0:
					System.out.println("BYE BYE!");
					break;
				default:
					System.out.println("Opci� incorrecta!");
			}
		}
	}
	
	private void connect() throws SQLException {
        String url = "jdbc:mysql://localhost:3306/onomastica";
        String user = "root";
        String passwd = "";
        connection = DriverManager.getConnection(url, user, passwd);
    }

    private void disconnect() {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                ;
            }
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                ;
            }
        }
    }
	
	public void insereixStatement() throws SQLException {
		try {
            connect();
            insereix = connection.prepareStatement("INSERT INTO noms2012(posicio, nom, sexe, frequencia, percentatge)"
            									 	+ "VALUES (?, ?, ?, ?, ?)");
            
            int n = 0;
            String s;
            String[] dades = null;
            
            try (BufferedReader lector = new BufferedReader(new FileReader(PATH_FITXER))) {
            	while ((s=lector.readLine()) != null) {
            		dades = s.split(";");
            		
            		insereix.setInt(1, Integer.parseInt(dades[0]));
            		insereix.setString(2, dades[1]);
            		insereix.setString(3, dades[2]);
            		insereix.setInt(4, Integer.parseInt(dades[3]));
            		insereix.setFloat(5, Float.parseFloat(dades[4].replace(',', '.')));
            		
            		n += insereix.executeUpdate();
            	}
            	System.out.println("S'han inserit " + n + " files.");
            } catch (IOException e) {
            	System.err.println(e.getMessage());
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            disconnect();
        }
	}
	
	public void insereixConsultaSQL() throws SQLException {
		try {
            connect();
            statement = connection.createStatement();
            
            int n = 0;
            String s;
            String[] dades = null;
            
            try (BufferedReader lector = new BufferedReader(new FileReader(PATH_FITXER))) {
            	while ((s=lector.readLine()) != null) {
            		dades = s.split(";");
            		n += statement.executeUpdate("INSERT INTO noms2012(posicio, nom, sexe, frequencia, percentatge)"
            				+ "VALUES("+dades[0]+", '"+dades[1]+"', '"+dades[2]+"', "
            				+			dades[3]+", "+Float.parseFloat(dades[4].replace(',', '.'))+")");
            	}
            	System.out.println("S'han inserit " + n + " files.");
            } catch (IOException e) {
            	System.err.println(e.getMessage());
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            disconnect();
        }
	}
	
	public void insereixPreparedStatement() throws SQLException {
		try {
            connect();
            statement = connection.createStatement();
            
            int n = 0;
            String s;
            String[] dades = null;
            String consulta = new String();
            
            try (BufferedReader lector = new BufferedReader(new FileReader(PATH_FITXER))) {
            	while ((s=lector.readLine()) != null) {
            		dades = s.split(";");
            		consulta +=  "INSERT INTO noms2012(posicio, nom, sexe, frequencia, percentatge)"
            				+ "VALUES("+dades[0]+", '"+dades[1]+"', '"+dades[2]+"', "
            				+			dades[3]+", "+Float.parseFloat(dades[4].replace(',', '.'))+"); ";
            	}
            	n += statement.executeUpdate(consulta);
            	System.out.println("S'han inserit " + n + " files.");
            } catch (IOException e) {
            	System.err.println(e.getMessage());
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            disconnect();
        }
	}
	
	public void esborraTot() throws SQLException {
		try {
            connect();
            statement = connection.createStatement();
            int n = 0;
            
            n += statement.executeUpdate("DELETE FROM noms2012");
            System.out.println("S'han esborrat " + n + " files.");
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            disconnect();
        }
	}
	
}
