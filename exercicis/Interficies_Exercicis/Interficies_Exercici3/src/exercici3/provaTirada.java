package exercici3;

public class provaTirada {

	public static void main(String[] args) {
		Tirada tirada1 = new Tirada(3, 1, 5);
		Tirada tirada2 = new Tirada(4, 2, 4);
		tirada1.toString();
		tirada2.toString();
	
		int res1 = tirada1.compareTo(tirada2);
		System.out.println("Comparacio de les dues: " + res1);
		
		Tirada tirada3 = new Tirada(tirada2);
		tirada2.toString();
		tirada3.toString();
		int res2 = tirada2.compareTo(tirada3);
		System.out.println("Comparacio amb una d'igual: " + res2);
	}

}
