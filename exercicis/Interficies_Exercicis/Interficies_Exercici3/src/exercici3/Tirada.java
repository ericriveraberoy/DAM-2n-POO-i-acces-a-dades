package exercici3;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class Tirada implements Comparable<Tirada>, Cloneable {
	private int atac; // Atac del jugador que fa la tirada
	private int defensa; // Defensa del jugador que fa la tirada
	private int numDaus; // Nombre de daus que te el jugador que fa la tirada
	private int[] resultatTirades; // Array amb el resultat de cada dau
	
	/**
	 * [Constructor] Tirada ha de rebre un atac, una defensa i un nombre de daus (tirades que fara).
	 * Amb aquests parametres generara el resultat de les tirades del jugador (dins del array resultatTirades[])
	 * @param atac
	 * @param defensa
	 * @param numDaus
	 */
	public Tirada(int atac, int defensa, int numDaus) {
		this.atac = atac;
		this.defensa = defensa;
		this.numDaus = numDaus;	
		this.resultatTirades = new int[numDaus];
		
		for (int i=0; i < numDaus; i++) {
			if (i >= numDaus)
				resultatTirades[i] = 0;
			else
				resultatTirades[i] = ThreadLocalRandom.current().nextInt(1, 6 + 1);
		}
	}
	
	public Tirada (Tirada t) {
		this.atac = t.atac;
		this.defensa = t.defensa;
		this.numDaus = t.numDaus;
		this.resultatTirades = t.resultatTirades;
	}
	
	/**
	 * toString retorna un string amb un "resum" de les tirades del jugador i, a m�s, mostra aquest "resum" per pantalla.
	 */
	public String toString () {
		StringBuilder stringBuilderResDaus = new StringBuilder();
		String stringResultatTirades = new String();
		
		for (int resultatTirada : resultatTirades)
			stringBuilderResDaus.append(resultatTirada);
			
		stringResultatTirades = "AT: " + atac + "  DEF: " + defensa + "  Daus (" + numDaus + "): " + stringBuilderResDaus;
		System.out.println(stringResultatTirades);
		
		return stringResultatTirades;
	}
	
	/**
	 * compareTo rep per parametre un objecte de tipus 'Tirada' (la del oponent)
	 * i la compara mitjan�ant l'array resultatTirades[]
	 * @param tiradaOponent
	 */
	@Override
	public int compareTo(Tirada tiradaOponent) {
		// El primer que he de fer es ordenar (descendent) els arrays resultatTirades[]
		Arrays.sort(this.resultatTirades);
		Arrays.sort(tiradaOponent.resultatTirades);
		
		// Un cop els tinc ordenats, comparo els resultats del mateix index de la seguent manera:
		//		Ara els comparo
		int puntuacio = 0;
		if (this.numDaus > tiradaOponent.numDaus) {
			for (int i=0; i < tiradaOponent.numDaus; i++) {
				if ((this.resultatTirades[i] + this.atac) > (tiradaOponent.resultatTirades[i] + tiradaOponent.defensa))
					puntuacio++;
			}
			puntuacio = puntuacio + (this.numDaus - tiradaOponent.numDaus);
		} else {
			for (int i=0; i < this.numDaus; i++) {
				if ((this.resultatTirades[i] + this.atac) > (tiradaOponent.resultatTirades[i] + tiradaOponent.defensa))
					puntuacio++;
			}
			puntuacio = puntuacio + (this.numDaus - tiradaOponent.numDaus);	
		}
		
		return puntuacio;
	}
}
