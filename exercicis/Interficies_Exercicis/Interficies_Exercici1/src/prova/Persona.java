package prova;

import java.util.Comparator;

/*
 Creeu la classe Persona amb els atributs pes, edat i al�ada.
 Implementeu la interf�cie Comparable per fer que es compari l'al�ada
 */

public class Persona implements Comparable<Persona> {
	private int pes;
	private int alcada;
	private int edat;
	
	public Persona(int pes, int alcada, int edat) {
		this.pes = pes;
		this.alcada = alcada;
		this.edat = edat;
	}
	
	public int getPes() {
		return pes;
	}
	public void setPes(int pes) {
		this.pes = pes;
	}
	public int getAlcada() {
		return alcada;
	}
	public void setAlcada(int alcada) {
		this.alcada = alcada;
	}
	public int getEdat() {
		return edat;
	}
	public void setEdat(int edat) {
		this.edat = edat;
	}
	
	public int compareTo(Persona p) {
		if (getAlcada() > p.getAlcada())
			return 1;
		else if (getAlcada() < p.getAlcada())
			return -1;
		else return 0;
	}
	
	public static final Comparator<Persona> COMPARATOR_EDAT = (p1, p2) -> p1.getEdat() - p2.getEdat();
	public static final Comparator<Persona> COMPARATOR_PES = (p1, p2) -> p1.getPes() - p2.getPes();
}
