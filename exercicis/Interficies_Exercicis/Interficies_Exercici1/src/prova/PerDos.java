package prova;

/*
 * Corregido: Faltaria un constructor que llamara a inicialitza();
 * 			  seguentNombre tendria que, primero hacer nombre = nombre*2 y luego return nombre
 */

public class PerDos implements Serie {
	private double nombre;

	public double getNombre() {
		return nombre;
	}

	public void nombreInicial(double d) {
		nombre = d;
	}

	public void inicialitza() {
		nombre = 1;
	}

	public double seguentNombre() {
		return nombre*2;
	}
}
