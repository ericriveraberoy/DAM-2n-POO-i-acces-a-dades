package prova;

import java.util.Arrays;
import java.util.Random;

public class Main {
/*
	public static void mostraVintPrimers (Serie s, double d){
		s.nombreInicial(d);
		System.out.println(d);
		for (int i=1; i<20; i++){
			double n;
			
			n = s.seguentNombre();
			s.nombreInicial(n);
			
			System.out.println(n);
		}
	}
	
	public static void main(String[] args) {
		Serie serie1 = new PerDos();
		mostraVintPrimers(serie1, 10);
	}
*/
	public static void main(String[] args) {
		Main main = new Main();
		main.mostraPersones();
		
		// Ordenem per pes
		Arrays.sort(main.persones, Persona.COMPARATOR_PES);
		main.mostraPersones();
		
		// Ordenem per edat
		Arrays.sort(main.persones, Persona.COMPARATOR_EDAT);
		main.mostraPersones();
	}
	
	
	private static final int N_PERSONES = 10;
	private static final Random random = new Random();
	private Persona[] persones = new Persona[N_PERSONES]; // Persona(int pes, int alcada, int edat)
	
	public Main() {
		// Creo 10 persones dintre d'un array de persones amb dades random
		for (int i=0; i<persones.length; i++) {
			persones[i] = new Persona(random.nextInt((120 - 40) + 1) + 40, random.nextInt((220 - 130) + 1) + 130, random.nextInt((95 - 1) + 1) + 1);
		}
	}
	
	public void mostraPersones() {
		for (Persona v : persones) 
			System.out.println("Pes: " + v.getPes() + ". Edat: " + v.getEdat());
		System.out.println();
	}
}
