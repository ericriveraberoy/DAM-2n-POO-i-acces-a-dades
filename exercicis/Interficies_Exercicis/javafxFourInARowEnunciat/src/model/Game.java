package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

public class Game {
	public static final int WIDTH = 7;
	public static final int HEIGHT = 6;

	private ObjectProperty<Owner> turn = new SimpleObjectProperty<>(Owner.BLUE);
	@SuppressWarnings("unchecked")
	private ObjectProperty<Owner>[][] board = new ObjectProperty[HEIGHT][WIDTH];
	private IntegerProperty[] colFills = new SimpleIntegerProperty[WIDTH];
	private IntegerProperty nTurns = new SimpleIntegerProperty(0);
	private ObjectProperty<Owner> winner = new SimpleObjectProperty<>(Owner.NONE);

	public Game() {
		for (int i = 0; i < HEIGHT; i++) {
			for (int j = 0; j < WIDTH; j++) {
				board[i][j] = new SimpleObjectProperty<>(Owner.NONE);
			}
		}
		for (int i = 0; i < WIDTH; i++) {
			colFills[i] = new SimpleIntegerProperty(HEIGHT - 1);
		}
	}

	public void newGame() {
		winner.set(Owner.NONE);
		turn.set(Owner.BLUE);
		nTurns.set(0);
		for (int i = 0; i < HEIGHT; i++) {
			for (int j = 0; j < WIDTH; j++) {
				board[i][j].set(Owner.NONE);
			}
		}
		for (int i = 0; i < WIDTH; i++) {
			colFills[i].set(HEIGHT - 1);
		}
	}

	public Owner getTurn() {
		return turn.get();
	}

	public ObjectProperty<Owner> getTurnProperty() {
		return turn;
	}

	public void nextTurn() {
		/*
		 * TODO: utilitza el metode next() de Owner
		 * per passar el torn al seguent jugador.
		 */
		turn.set(turn.get().next());
		
		
		/*
		 * TODO: suma 1 al numero total de torns
		 * transcorreguts (nTurns).
		 */
		nTurns.set(nTurns.get()+1);
		
	}

	public ObjectProperty<Owner> getBoardProperty(int row, int col) {
		return board[row][col];
	}

	public Owner getBoard(int row, int col) {
		return board[row][col].get();
	}

	public void setBoard(Owner owner, int row, int col) {
		board[row][col].set(owner);
	}

	public int getColFills(int col) {
		return colFills[col].get();
	}

	public void decreaseColFills(int col) {
		colFills[col].set(colFills[col].get() - 1);
	}

	public IntegerProperty getColFillsProperty(int col) {
		return colFills[col];
	}

	public int getNTurns() {
		return nTurns.get();
	}

	public IntegerProperty getNTurnsProperty() {
		return nTurns;
	}
	
	public Owner getWinner() {
		return winner.get();
	}
	
	public ObjectProperty<Owner> getWinnerProperty() {
		return winner;
	}

	public void play(int col) {
		int row = getColFills(col);
		setBoard(getTurn(), row, col);
		if (checkVictory(row, col))
			winner.set(getTurn());
		decreaseColFills(col);
		nextTurn();
	}

	private int countCells(Owner owner, int row, int col, int drow, int dcol) {
		int r = row, c = col, inline = 0;

		while (c >= 0 && c < WIDTH && r >= 0 && r < HEIGHT && board[r][c].get() == owner) {
			inline++;
			c += dcol;
			r += drow;
		}
		return inline;
	}

	private boolean checkVictory(int row, int col, int drow, int dcol) {
		int inline = countCells(board[row][col].get(), row, col, drow, dcol);
		inline += countCells(board[row][col].get(), row - drow, col - dcol, -drow, -dcol);

		return inline >= 4;
	}

	private boolean checkVictory(int row, int col) {
		return checkVictory(row, col, 0, 1) || checkVictory(row, col, 1, 0) || checkVictory(row, col, 1, 1)
				|| checkVictory(row, col, 1, -1);
	}
}
