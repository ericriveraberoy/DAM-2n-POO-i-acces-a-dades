package view;

import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import model.Game;
import model.Owner;

public class FourInARow extends Application implements EventHandler<ActionEvent> {
	private Button newGameButton = new Button("Nova partida");
	private BoardSquare[][] boardSquares = new BoardSquare[Game.HEIGHT][Game.WIDTH];
	private PlayButton[] playButtons = new PlayButton[Game.WIDTH];
	private Label infoLabel;
	private BoardSquare turnSquare = new BoardSquare();
	
	private Game game = new Game();

	public static void main(String[] args) {
		launch(args);
	}
	
	private Node createTopNode() {
		HBox topNode = new HBox();
		topNode.setPadding(new Insets(5, 5, 5, 5));
		topNode.getChildren().add(newGameButton);
		return topNode;
	}
	
	private Node createCenterNode() {
		GridPane centerNode = new GridPane();
		/*
		 * TODO: Crea la graella de botons central. Els botons
		 * s'han de guardar a boardSquares i s'han d'afegir a
		 * centerNode.
		 */
		for (int col = 0; col < Game.WIDTH; col++) {
			for (int fil = 0; fil < Game.HEIGHT; fil++) {
				boardSquares[fil][col] = new BoardSquare();
				centerNode.add(boardSquares[fil][col], col, fil);
			}
		}
		
		
		/*
		 * TODO: Crea els botons de playButtons. Hi ha d'haver un
		 * boto per cada columna. Afegeix els botons a l'ultima
		 * fila de centerNode.
		 */
		for (int col = 0; col < Game.WIDTH; col++) {
			playButtons[col] = new PlayButton(col);
			centerNode.add(playButtons[col], col, Game.HEIGHT);
		}
		
		return centerNode;
	}
	
	private Node createRightNode() {
		VBox rightNode = new VBox();
		infoLabel = new Label("Torn del jugador:");
		infoLabel.setPrefWidth(120);
		infoLabel.setAlignment(Pos.CENTER);
		rightNode.setPadding(new Insets(5, 5, 10, 10));
		rightNode.setSpacing(10);
		rightNode.getChildren().addAll(infoLabel, turnSquare);
		rightNode.setAlignment(Pos.TOP_CENTER);
		return rightNode;
	}
	
	private BorderPane createBorderPane() {
		BorderPane borderPane = new BorderPane();
		borderPane.setTop(createTopNode());
		borderPane.setRight(createRightNode());
		borderPane.setCenter(createCenterNode());
		return borderPane;
	}

	@Override
	public void start(Stage primaryStage) {
		BorderPane root = createBorderPane();
		setBindings();
		setEventHandlers();
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.setResizable(false);
		primaryStage.setTitle("4 en ratlla");
		primaryStage.show();
	}

	private void setBindings() {
		turnSquare.getOwnerProperty().bind(
			Bindings.when(game.getWinnerProperty().isNotEqualTo(Owner.NONE))
			.then(game.getWinnerProperty())
			.otherwise(
				Bindings.when(game.getNTurnsProperty().greaterThanOrEqualTo(Game.HEIGHT*Game.WIDTH))
				.then(Owner.NONE)
				.otherwise(game.getTurnProperty())
			)
		);
		
		/*
		 * TODO: assigna el propietari de cadascuna de les caselles centrals (boardSquares)
		 * per tal que sempre coincideixi amb el propietari de la casella equivalent en
		 * el tauler del model. Pots obtenir cadascuna de les caselles del model amb 
		 * game.getBoardProperty(). 
		 */
		for (int col = 0; col < Game.WIDTH; col++) {
			for (int fil = 0; fil < Game.HEIGHT; fil++) {
				boardSquares[fil][col].getOwnerProperty().bind(game.getBoardProperty(fil, col));
			}
		}
		
		/*
		 * TODO: per a cada boto de playButtons, fes que estigui deshabilitat quan la columna on
		 * es estigui plena o quan ja hi hagi un guanyador de la partida. game.getColFillsProperty()
		 * es la propietat que indica quantes caselles queden per omplir en una columna.
		 * game.getWinnerProperty() es la propietat que inidica el guanyador de la partida.
		 */
		for (int col = 0; col < Game.WIDTH; col++) {
			playButtons[col].disableProperty().bind(
					Bindings.lessThan(game.getColFillsProperty(col), 0)
					.or(Bindings.notEqual(game.getWinnerProperty(), Owner.NONE))
				);
		}
		
		
		/*
		 * TODO: el text d'infoLabel ha de modificar-se segons la situacio de la partida. Si hi ha
		 * un guanyador ha de mostrar el text "Victoria de:". Si s'ha jugat a totes les caselles
		 * ha de mostrar "Es un empat!". Si encara s'esta jugant ha de mostrar "Torn del jugador:".
		 * Utilitza un esquema similar al binding que tens d'exemple al principi d'aquest
		 * metode.
		 */
		infoLabel.textProperty().bind(
				Bindings.when(game.getWinnerProperty().isNotEqualTo(Owner.NONE))
				.then("Victoria de:")
				.otherwise(
					Bindings.when(game.getNTurnsProperty().greaterThanOrEqualTo(Game.WIDTH*Game.HEIGHT))
					.then("Es un empat!")
					.otherwise("Torn del jugador:")
				)
			);
		
	}
	
	private void setEventHandlers() {
		for (int j=0; j<Game.WIDTH; j++) {
			playButtons[j].setOnAction(this);
		}
		newGameButton.setOnAction(event->game.newGame());
	}

	@Override
	public void handle(ActionEvent event) {
		/*
		 * TODO: obte el boto que s'ha premut ha partir de event.
		 * Crida a game.play() passant-li la columna corresponent a aquest boto. El propi boto
		 * ja sap a quina columna esta i pots recuperar-ho amb getCol().
		 */
		PlayButton btn = (PlayButton) event.getSource();
		game.play(btn.getCol());
	}
}
