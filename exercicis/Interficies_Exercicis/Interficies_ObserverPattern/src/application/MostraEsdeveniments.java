package application;

public class MostraEsdeveniments implements Listener {

	@Override
	public void notifyEvent(Integer a) {
		System.out.println("S'ha produit l'esdeveniment " + a);
	}

}
