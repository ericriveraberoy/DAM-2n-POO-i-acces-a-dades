package application;

import java.util.ArrayList;

public class GuardaEsdeveniments implements Listener {
	
	ArrayList<Integer> esdeveniments = new ArrayList<Integer>();

	@Override
	public void notifyEvent(Integer a) {
		esdeveniments.add(a);
	}

	
	@Override
	public String toString() {
		return esdeveniments.toString();
	}
}
