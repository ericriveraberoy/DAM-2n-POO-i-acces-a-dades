package application;

public class Principal {

	public static void main(String[] args) {
		
		GuardaEsdeveniments g = new GuardaEsdeveniments();
		MostraEsdeveniments m = new MostraEsdeveniments();
		ProdueixEsdeveniments p = new ProdueixEsdeveniments();
		
		p.addEventListener(g);
		p.addEventListener(m);
		
		p.creaEsdeveniment(4);
		p.creaEsdeveniment(7);
		p.creaEsdeveniment(3);
		p.creaEsdeveniment(5);
		
		System.out.println(g.toString());
	}

}
