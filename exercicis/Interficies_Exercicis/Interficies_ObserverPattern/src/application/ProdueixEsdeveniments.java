package application;

import java.util.ArrayList;

public class ProdueixEsdeveniments {
	
	ArrayList<Listener> listeners = new ArrayList<Listener>();
	
	public void addEventListener(Listener l) {
		listeners.add(l);
	}
	
	public void creaEsdeveniment(Integer a) {
		for (Listener listener : listeners)
			listener.notifyEvent(a);
	}
	
}
