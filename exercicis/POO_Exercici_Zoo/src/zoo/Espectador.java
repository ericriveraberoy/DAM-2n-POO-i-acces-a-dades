package zoo;

public class Espectador implements Esser {

	@Override
	public String accio(Zoo z) {
		Animal a = z.mostraAnimal();
		if (a == null) {
			return "Un espectador no sap a on mirar perqu� no troba animals";
		} else {
			if (a instanceof Vaca) {
				return "Un espectador mira una vaca";
			} else if (a instanceof Cocodril) {
				return "Un espectador mira a un perill�s cocodril";
			} else return "";
		}
	}

}
