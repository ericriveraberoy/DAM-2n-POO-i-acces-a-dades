package zoo;

import java.util.ArrayList;
import java.util.Random;

public class Zoo {
	private ArrayList<Animal> animals = new ArrayList<Animal>();
	private Espectador espectador;
	
	public Animal mostraAnimal() {
		if (animals.isEmpty()) {
			return null;
		} else {
			Random r = new Random();
			return animals.get(r.nextInt(animals.size()+1));
		}
	}
	
	public void afegeixAnimal (String s) {
		if (s.equals("vaca")) {
			animals.add(new Vaca());
		} else if (s.equals("cocodril")) {
			animals.add(new Cocodril());
		}
	}
	
	public void suprimeixAnimal (String s) {
		boolean esborrat = false;
		for (int i = 0; i < animals.size(); i++) {
			if (s.equals("vaca")) {
				if (animals.get(i) instanceof Vaca && !esborrat) {
					animals.remove(i);
					esborrat = true;
				}
			} else if (s.equals("cocodril")) {
				if (animals.get(i) instanceof Cocodril && !esborrat) {
					animals.remove(i);
					esborrat = true;
				}
			}
		}
	}
	
	public void suprimeixAnimal (Animal a) {
		animals.remove(animals.indexOf(a));
	}
	
	public void suprimeixTots (String s) {
		for (int i = 0; i < animals.size(); i++) {
			if (s.equals("vaca")) {
				if (animals.get(i) instanceof Vaca) {
					animals.remove(i);
				}
			} else if (s.equals("cocodril")) {
				if (animals.get(i) instanceof Cocodril) {
					animals.remove(i);
				}
			}
		}
	}
	
	public void mira () {
		Random r = new Random();
		
		if (r.nextInt(1) == 0) {
			System.out.println(espectador.accio(this));
		} else {
			if (animals.isEmpty()) {
				System.out.println("Quin zoo m�s avorrit! No t� cap animal");
			} else {
				System.out.println(animals.get(r.nextInt(animals.size()+1)).accio(this));
			}
		}
	}
	
}
