package zoo;

public class Cocodril extends Animal {
	
	public String mou(Zoo z) {
		return "Un cocodril neda estany amunt estany avall";
	}

	public String alimenta(Zoo z) {
		Animal a = z.mostraAnimal();
		if (a == null || a == this) {
			return "Un cocodril busca a qui es pot menjar";
		} else {
			z.suprimeixAnimal(a);
			if (a instanceof Vaca) {
				return "Un cocodril es menja una vaca!";
			} else if (a instanceof Cocodril) {
				return "Un cocodril es menja un altre cocodril";
			} else {
				return "";
			}
		}
	}

	public String expressa(Zoo z) {
		return "Un cocodril obre una boca plena de dents";
	}
}
