package zoo;

public class Vaca extends Animal {
	
	public String mou(Zoo z) {
		return "Una vaca es posa a dormir";
	}

	public String alimenta(Zoo z) {
		return "Una vaca comen�a a pastar amb tranquil�litat";
	}

	public String expressa(Zoo z) {
		return "Una vaca fa mu";
	}
}
