package ex4;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/*
	Exercici 4. Versi� simple del tee
		Fes un programa que guardi tots els car�cters que rebi per l'entrada est�ndard
		a un fitxer el nom del qual es rep per par�metre i que a m�s els mostri per pantalla.
		El programa ha d'actuar car�cter a car�cter.
*/

public class Main {

	public static void main(String[] args) {
		if (args.length > 0) {
			String pathFitxer = new String(args[0]);

			Scanner sc = new Scanner(System.in);
			System.out.println("Frase: ");
			String frase = sc.nextLine();
			
			try (FileWriter escriptor = new FileWriter(pathFitxer)) {
				for (char ch : frase.toCharArray()) {
					escriptor.append(ch);
				}
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
			
			
		} else {
			System.out.println("Has de pasar per parametre el fitxer on guardar la sortida!");
		}
	}

}
