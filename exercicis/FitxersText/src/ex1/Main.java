package ex1;

import java.io.FileReader;
import java.io.IOException;

/*
	Exercici 1. Comptar a
		Fes un programa que demani un nom de fitxer a l'usuari, i mostri quantes lletres 'a' hi ha al fitxer.
*/


public class Main {

	public static void main(String[] args) {
		String nomFitxer = "C:\\Users\\ies\\Desktop\\prova.txt";
		int lectura;
        char ch;
        int contador = 0;
		
		try (FileReader lector = new FileReader(nomFitxer)) {
			while ((lectura = lector.read()) != -1) {
				ch = (char) lectura;
				if (ch == 'a') {
					contador++;
				}
			}
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		System.out.println("Hi ha " + contador + " lletres 'a'.");
	}

}
