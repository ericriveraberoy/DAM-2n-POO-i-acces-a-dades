package ex5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		String dir = System.getProperty("user.dir");
		String op = new String();
		
		Scanner sc = new Scanner(System.in);
		while (!op.equals("surt")) {
			System.out.println(dir + "> ");
			op = sc.nextLine();
			
			if (op.equals("ls")) {
				ls(dir);
			} else if (op.substring(0, 3).equals("cd ")) {
				dir = cd(dir, op.substring(3, op.length()));
			} else if (op.substring(0, 4).equals("cat ")) {
				cat(op.substring(4, op.length()));
			} else if (op.equals("surt")){
				System.out.println("Bye bye!");
			} else {
				System.out.println("Comanda inv�lida! Es permet 'ls', 'cd' i 'cat'");
			}
		}
		sc.close();
	}

	public static void ls (String sd) {
		ArrayList<String> arrayStringDirs = new ArrayList<String>();
		arrayStringDirs.add(sd);
		
		while (!arrayStringDirs.isEmpty()) {
			Path dir = Paths.get(arrayStringDirs.get(0));
            System.out.println("Fitxers del directori " + dir + ":");
			if (Files.isDirectory(dir)) {
				try(DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
					for (Path fitxer: stream) {
						if (Files.isDirectory(fitxer)) {
							arrayStringDirs.add(fitxer.toString());
						}
						if(fitxer.toFile().canRead()) System.out.print("r");
						else System.out.print("-");
						
						if(fitxer.toFile().canWrite()) System.out.print("w");
						else System.out.print("-");
						
						if(fitxer.toFile().canExecute()) System.out.print("x");
						else System.out.print("-");
						
						System.out.print("   " + fitxer.getFileName() + "\n");
                    }
				} catch(IOException | DirectoryIteratorException ex) {
					System.err.println(ex);
				}
			} else {
				System.err.println(dir.toString() + " no �s un directori");
			}
			arrayStringDirs.remove(0);
		}
	}
	
	public static String cd (String sd, String sd2) {
		if (Files.isDirectory(Paths.get(sd2))) {
			return sd2;
		} else {
			System.out.println("'" + sd2 + "' no es un directori valid!");
			return sd;
		}
	}
	
	public static void cat (String sd) {
		if (Files.isRegularFile(Paths.get(sd))) {
			String s;
			try (BufferedReader lector = new BufferedReader(new FileReader(sd))) {
				while ((s = lector.readLine()) != null) {
					System.out.println(s);
				}
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
		}
	}
	
}
