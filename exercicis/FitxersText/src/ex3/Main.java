package ex3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {
	public static void main(String[] args) {
		if (args.length >= 2) {
			String paraula = new String(args[0]);
			/*
			 * Recorregut pels arguments que li passem (que seran rutes a directoris o fitxers)
			 */
			for (int i = 1; i < args.length; i++) {
				/*
				 * Si es un directori, busquem coincidencies
				 */
				if (Files.isRegularFile(Paths.get(args[i]))) {
					buscaParaulaFitxer(paraula, args[i]);
				} else if (Files.isDirectory(Paths.get(args[i]))) {
					/*
					 * Si es un directori, per cada fitxer dins del directori, busquem coincidencies
					 */
					try(DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(args[i]))) {
						for (Path fitxer : stream) {
							if (Files.isRegularFile(fitxer)) {
								buscaParaulaFitxer(paraula, fitxer.toString());
							}
						}
					} catch(IOException | DirectoryIteratorException ex) {
						System.err.println(ex);
					}
				} else {
					System.out.println(args[i] + ": No es ni un directori ni un fitxer!");
				}
			}
		} else {
			System.out.println("Falten arguments");
		}
	}
	
	public static void buscaParaulaFitxer(String paraula, String pathFitxer) {
		String s;
		try (BufferedReader lector = new BufferedReader(new FileReader(pathFitxer))) {
			int linia = 1;
			while ((s = lector.readLine()) != null) {
				if (s.contains(paraula)) {
					System.out.println(pathFitxer + ": Linia " + linia);
				}
				linia++;
			}
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
}
