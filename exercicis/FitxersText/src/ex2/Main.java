package ex2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
	 Exercici 2. Versi� simple del grep
		Fes un programa que rebi per par�metre una paraula i un fitxer i que mostri
		per pantalla nom�s aquelles l�nies del fitxer que contenen la paraula.
*/

public class Main {

	public static void main(String[] args) {
		String s;
		String paraula = "Eric";
		int count = 0;

		String total = new String();

		try (BufferedReader lector = new BufferedReader(new FileReader("C:\\Users\\ies\\Desktop\\prova.txt"))) {
			while ((s = lector.readLine()) != null) {
				total += s + "\n";
			}
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		
	    Pattern p = Pattern.compile(paraula);
	    Matcher m = p.matcher(total);
	    while (m.find()){
	    	count +=1;
	    }
	    System.out.println(count);
	}

}
