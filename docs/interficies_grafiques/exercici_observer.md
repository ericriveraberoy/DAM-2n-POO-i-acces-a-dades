### Exercici Observer Pattern

En aquest exercici, veurem com una classe que produeix esdeveniments (com un
botó quan l'usuari fa clic a sobre seu), pot notificar a altres objectes per tal
que aquests actuïn en conseqüència.

* Crea la interfície *Listener* amb el mètode *notifyEvent*. Aquest mètode
serà cridat cada vegada que es produeixi un esdeveniment per tots aquells
objectes que estiguin esperant esdeveniments. El mètode rep un nombre enter,
que representarà el tipus d'esdeveniment que es produeix.

* Crea la classe *GuardaEsdeveniments*. Aquesta classe implementarà la
interfície *Listener*, i cada vegada que rebi un esdeveniment el guardarà en
una llista. Sobreescriu el mètode *toString* per tal que retorni una cadena
amb el contingut de la llista.

* Crea la classe *MostraEsdeveniments*. Aquesta classe implementarà la
interfície *Listener*, i cada vegada que rebi un esdeveniment mostrarà per
pantalla quin esdeveniment s'ha produït (és a dir, el seu nombre).

* Crea la classe *ProdueixEsdeveniments*. Aquesta classe conté una llista de
*Listener*.

* Crea el mètode *addEventListener* a la classe *ProdueixEsdeveniments*.
Aquest mètode rep un *Listener* i l'afegeix a la llista.

* Crea el mètode *creaEsdeveniment* a la classe *ProdueixEsdeveniments*.
Aquest mètode rep un nombre (l'esdeveniment) i crida al mètode *notifyEvent*
de tots els *Listener* de la llista, passant aquest nombre com a paràmetre.

* Crea el mètode *main* a la classe *Principal*. En aquest mètode, crea un
objecte *GuardaEsdeveniments*, un objecte *MostraEsdeveniments* i un objecte
*ProdueixEsdeveniments*. Afegeix els dos primers objectes com a *Listener* de
l'objecte *ProdueixEsdeveniments* (utilitzant el mètode *addEventListener*).

    Crida diverses vegades el mètode *creaEsdeveniment* de l'objecte
*ProdueixEsdeveniments*, passant diferents valors. Finalment, visualitza el
contingut de l'objecte *GuardaEsdeveniments* per pantalla.

El resultat d'executar el programa tindrà aquest aspecte:

```
S'ha produït l'esdeveniment 3
S'ha produït l'esdeveniment 6
S'ha produït l'esdeveniment 2
3 6 2
```

#### Quin sentit té aquest exercici?  

Imaginem que estem implementant una interfície gràfica que conté una
llista amb una sèrie d'elements seleccionables. Quan l'usuari selecciona
un element, volem que es mostri la informació d'aquest element a un
quadre de text, i volem que algunes opcions del menú s'activin o es
desactivin en funció de l'element seleccionat.  

Tot això ho volem fer mantenint la independència entre el quadre de
text, el menú, i la llista. És a dir, no volem que des del codi de la
llista es cridi directament a mètodes del quadre de text i del menú, Per
què?

1. Perquè els responsables de saber com s'han de configurar són el propi menú
i el propi quadre de text, no la llista, que no té per que conèixer els
detalls interns dels altres controls.

2. Perquè volem reutilitzar la nostra llista a altres diàlegs, que potser no
tindran menú o quadre de text.

3. Perquè en algun moment potser voldrem afegir algun altre control que hagi
de reaccionar als esdeveniments de la llista, i quan això passi, no volem
haver de modificar el codi de la llista.

Per resoldre aquest problema procedim com a l'exercici: la llista fa el
paper de *ProdueixEsdeveniments* (és la font dels esdeveniments, que es
produeixen quan l'usuari selecciona un element).

La llista desplegable contindrà una llista amb tots els seus *Listener*:
el menú i el quadre de text.

Però tal com està dissenyat, no sabrà qui són i no els tindrà
codificats directament. Només sabrà que implementen la interfície
*Listener*, i el codi d'inicialització del diàleg serà qui haurà fet
l'associació a través de la crida del mètode *addEventListener* de la
llista desplegable (això té sentit, perquè el diàleg sí que sap quins
elements ha de contenir i com es relacionen entre ells).

Com s'ha vist, tant el menú com el quadre de text han d'implementar
la interfície *Listener*: fan el paper dels *MostraEsdeveniments* i
*GuardaEsdeveniments* de l'exercici.

De tot això se n'anomena **Observer Pattern**.

**Diagrama de classes de l'exercici**:

![diagrama de classes](docs/interficies_grafiques/imatges/exercici_interficies_observer_pattern.png)

**Diagrama de seqüència de l'exercici**:

![diagrama de seqüència](docs/interficies_grafiques/imatges/exercici_interficies_observer_pattern_sequencia.png)
