## Examen

### Instruccions

Pels diagrames de classes cal indicar:

- La multiplicitat de totes les relacions i propietats.
- El tipus de les propietats.
- El tipus de retorn de les operacions i del seus paràmetres.
- La visibilitat de propietats i operacions.

Per a cada exercici cal entregar el codi font del diagrama en format del
*PlantUML* i una imatge del diagrama en format *png*.

### Exercici 1

**5 punts**

Fes un diagrama de classes que modelitzi la següent situació.

Per a un sistema informàtic volem crear un sistema de permisos que permeti
només a alguns usuaris realitzar certes accions. Per fer-ho tindrem una sèrie
de rols definits que podran tenir els usuaris. Quan un usuari vulgui
realitzar una acció es comprovarà si disposa d'algun dels rols que li permet
dur a terme aquesta acció.

Detalladament, tenim les següents restriccions:

1. El nostre sistema tindrà forces usuaris. De cada usuari en guardarem un
identificador, un nom d'usuari, el seu nom i cognoms, i un o més correus
electrònics de contacte.

2. Tindrem diversos rols definits al sistema. A cadascun li assignarem un nom
i una breu descripció.

3. Un usuari tindrà almenys un rol, però en pot tenir més d'un. Donat un
usuari ha de ser fàcil saber quins rols té assignats.

4. En el nostre sistema es podran realitzar una sèrie d'activitats concretes,
a les quals assignarem un nom i un identificador.

5. Una activitat es pot realitzar per qualsevol usuari que tingui almenys un
dels rols associats a l'activitat. Sempre existirà almenys un rol que permet
realitzar una activitat. Donada una activitat hem de poser saber immediatament
quins rols la permeten realitzar.

6. Per accedir a les diverses activitats del sistema disposarem d'un menú. Les
entrades del menú s'organitzen en forma d'arbre, com és habitual en els menús
de programes gràfics.

7. Cada entrada del menú té un codi únic i el text que es mostrarà. Les
entrades del menú poden estar relacionades amb una activitat, que servirà per
determinar quins usuaris hi tenen accés i quins no.

8. Per poder construir el menú en forma d'arbre, cada entrada del menú ha de
saber quines són les seves subentrades.

9. Els usuaris han de tenir una operació que permeti saber si poden o no
realitzar una certa activitat.

[Solució exercici 1](codi/diagrames_uml/diagrames_classes/examen_ex1.puml)

### Exercici 2

**5 punts**

***Tria només un dels dos exercicis següents***

### Exercici 2a

A partir de la solució que teniu penjada per a l'exercici del jardí, feu el
seu diagrama de classes.

No cal posar les propietats *private*, ni els constructors, ni les
multiplicitats. Cal posar els paràmetres i tipus de retorn dels mètodes i
les relacions entre les classes.

[Solució exercici 2a](codi/diagrames_uml/diagrames_classes/examen_ex2a.puml)

### Exercici 2b

Es proporciona un esquelet de codi Java del programa per gestionar una
biblioteca. Ens hem centrat en el cas d'ús per donar d'alta nous exemplars al
catàleg de la biblioteca. Cal suposar que el programa és més extens i que el
catàleg ja conté una colla d'exemplars amb els llibres corresponents.

A partir d'aquest codi genera un diagrama de seqüència on es vegi clarament
els passos que se segueixen a partir que es crida el mètode *altaLlibre* de
la classe *Biblioteca*.

Suposa que el llibre que es dóna d'alta no existeix encara al catàleg.

[Solució exercici 2b](codi/diagrames_uml/diagrames_sequencia/examen_ex2b.puml)
