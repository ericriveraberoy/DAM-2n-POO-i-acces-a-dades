## Interfície *Path*

La interfície *Path* representa un fitxer o directori en Java.

No necessàriament ha de correspondre amb un fitxer existent al sistema de
fitxers. També es pot utilitzar per comprovar si un fitxer existeix o no, o
per crear un fitxer nou, entre d'altres.

Noteu que la ruta d'un fitxer o directori és depenent del sistema operatiu.

Crearem un objecte *Path* a partir d'algun dels mètodes *factory* de la
classe *Paths*:

```java
Path fitxer1 = Paths.get("/home/usuari/exemple1.txt"); //Linux
Path fitxer2 = Paths.get("c:\\Users\\usuari\\exemple1.txt"); //Windows
```

En Windows, cal utilitzar una doble barra (\\) perquè el caràcter \ ja té un
significat especial dins d'una cadena en Java.

Podem accedir directament a fitxers/directoris que estiguin al directori de
l'usuari que està executant el nostre programa:

```java
Path fitxer = Paths.get(System.getProperty("user.home"), "dir", "fitxer.txt");
```

*fitxer* apuntaria a */home/usuari/dir/fitxer.txt* en Linux/MacOS, o a
*C:\Users\usuari\dir\fitxer.txt* en Windows.

La interfície *Path* ens proporciona una colla de mètodes útils per obtenir
informació de la ruta que hem especificat.

Per exemple, *getFileName()* ens retorna l'última part de la ruta, mentre
que *getParent()* ens retorna la ruta sencera excepte l'última part.

### Exemple: llistar el contingut d'un directori

```java
public class LListarDirectori {

	public static void main(String[] args) {		
		if (args.length == 1) {
			Path dir = Paths.get(args[0]);
			System.out.println("Fitxers del directori " + dir);
			if (Files.isDirectory(dir)) {
				try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
					for (Path fitxer: stream) {
						System.out.println(fitxer.getFileName());
					}
				} catch (IOException | DirectoryIteratorException ex) {		    
					System.err.println(ex);
				}
			} else {
				System.err.println(dir.toString()+" no és un directori");
			}
		} else {
			System.err.println("Ús: java LlistarDirectori <directori>");
		}
	}
}
```

### Exemple: crear i esborrar un fitxer

```java
public class CreaFitxer {
	public static void main(String[] args) {
		if (args.length == 1) {
			Path fitxer = Paths.get(args[0]);

			// Files.exists(fitxer)
			try (Scanner sc=new Scanner(System.in)) {
				try {
					Files.createFile(fitxer);
					System.out.println("S'ha creat el fitxer");
				} catch (FileAlreadyExistsException ex) {
					System.out.println("El fitxer ja existia");
				}
				System.out.println("Ruta absoluta: "+fitxer.toAbsolutePath());
				System.out.print("Vols esborrar el fitxer " + fitxer + "(s/N)? ");
				if (sc.nextLine().equalsIgnoreCase("s")) {
					Files.delete(fitxer);
					System.out.println("S'ha esborrat el fitxer.");
				} else {
					System.out.println("S'ha conservat el fitxer.");
				}
			} catch (IOException ex) {
				System.err.println(ex);
			}
		} else {
			System.err.println("Ús: java CreaFitxer <fitxer>");
		}
	}

}
```
