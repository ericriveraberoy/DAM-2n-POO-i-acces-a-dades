## Validació de documents XML

En aquest apartat veurem com podem validar un document XML des d'un programa
Java.

El nostre programa recorrerà el fitxer XML utilitzant DOM o SAX i tindrà
l'oportunitat de realitzar una acció cada vegada que es detecti un error en
el document.

Igual que hem fet amb la lectura d'un fitxer amb SAX, tindrem un controlador
(*handler*) amb una colla de mètodes que es cridaran quan es produeixin certs
esdeveniments. En aquest cas el controlador derivarà de *ErrorHandler* i els
esdeveniments seran del tipus *error()* o *warning()*.

El nostre controlador serà el mateix tant si llegim el document amb DOM com
amb SAX:

[ControladorErrades](codi/fitxers//ControladorErrades.java)

Com podeu veure, es tracta d'un controlador molt senzill que es limita a
informar dels errors trobats per pantalla.

Per validar el fitxer utilitzant DOM podem fer-ho de la següent manera:

[ValidadorDOM](codi/fitxers//ValidadorDOM.java)

**Línies 19 a 21**: Creem una *DocumentBuilderFactory* i li indiquem que
volem validar documents. Si utilitzem espais de noms (*namespaces*) també
li indiquem.

**Línies 22 i 23**: Creem un *DocumentBuilder* i li assignem el nostre
controlador d'errors.

**Línia 24**: Obtenim el fitxer XML i li passem a l'intèrpret. El propi
fitxer XML ha de fer referència al DTD que s'hagi d'utilitzar per a la
validació.

Per validar el fitxer amb SAX utilitzem un mecanisme similar:

[ValidadorSAX](codi/fitxers//ValidadorSAX.java)

**Línies 20 a 22**: Creem una *SAXParserFactory*, li indiquem volem validar
i, si és el cas, que utilitzem *namespaces*.

**Línies 23 a 24**: Creem un *SAXParser* i, d'ell, obtenim un *XMLReader*,
que serà l'objecte que finalment utilitzarem per llegir el document.

**Línies 25 a 26**: Assignem el nostre controlador d'errades i llegim el
document. El propi fitxer XML ha de fer referència al DTD que s'hagi
d'utilitzar per a la validació.
