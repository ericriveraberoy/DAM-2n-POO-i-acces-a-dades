## Sentències preparades

### SQL-Injection

Observem el següent programa:

```java
public class SentenciaSensePreparar {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String lastName=" ";
		try (Connection conn = DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/employees", "root", "super3");
				Statement st = conn.createStatement()) {
			while (!lastName.equals("")) {
				System.out.println("Cognom a cercar (en blanc per sortir): ");
				lastName = sc.nextLine();
				if (!lastName.equals("")) {
					try (ResultSet rs = st.executeQuery(
							"SELECT emp_no, first_name, last_name FROM employees "
							+ "WHERE last_name LIKE '"+lastName+"'")) {
						while (rs.next()) {
							int empNo = rs.getInt("emp_no");
							String firstName = rs.getString("first_name");
							lastName = rs.getString("last_name");
							System.out.println(empNo+"\t"+firstName+" "+lastName);
						}
					}
				}
			}
		} catch (SQLException e) {
			System.err.println("Error SQL: "+e.getMessage());
		}
		sc.close();
	}
}
```

Aquest programa demana a l'usuari un cognom i mostra el número i nom complet
dels empleats que tenen aquest cognom:

```
Cognom a cercar (en blanc per sortir):
Flanders
14908	Kerhong Flanders
16333	Kazunori Flanders
19595	Gift Flanders
21032	Leah Flanders
24501	Constantijn Flanders
...
```

Aparentment el programa funciona correctament, però l'usuari pot introduir
cadenes com:

```
Cognom a cercar (en blanc per sortir):
' or '1'='1
10001	Georgi ' or '1'='1
10002	Bezalel ' or '1'='1
10003	Parto ' or '1'='1
10004	Chirstian ' or '1'='1
10005	Kyoichi ' or '1'='1
10006	Anneke ' or '1'='1
10007	Tzvetan ' or '1'='1
10008	Saniya ' or '1'='1
10009	Sumant ' or '1'='1
10010	Duangkaew ' or '1'='1
```

L'usuari ha aconseguit mostrar la llista completa d'empleats.

Això és un problema greu de seguretat. Imaginem una consulta similar que tingui
per objectiu validar l'usuari i contrasenya d'un usuari:

```java
String query = "select name, country, password from Users where email = '"+
    id+"' and password='"+pwd+"'";
```

Si a aquesta consulta li envíem un usuari de l'estil
`prova@domini.com' or '1'='1` aconseguim validar l'accés sense necessitat de
tenir una parella de nom d'usuari i contrasenya vàlids.

### Ús de les sentències preparades

Per evitar aquests problemes, sempre que una sentència SQL depengui de
paràmetres que poden variar o que hem obtingut de fonts no fiables, hem
d'utilitzar una sentència preparada.

Les sentències preparades tenen els següents avantatges:

- Filtren els paràmetres variables de les sentències, evitant vulnerabilitats.

- Simplifiquen l'escriptura de les sentències, ja que no hem d'anar tancat i
obrint cometes i concatenant cadenes.

- Si una mateixa consulta es repeteix molts cops amb paràmetres diferents,
optimitzen la velocitat d'accés a les dades, perquè la sentència només
s'envia un cop al SGBD. El SGBD compila la sentència i la guarda, modificant
només els paràmetres necessaris cada vegada que s'executa la sentència.

El següent programa és la vesió amb sentències preparades del programa d'exemple
anterior:

```java
public class SentenciaPreparada {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String lastName=" ";
		try (Connection conn = DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/employees", "root", "super3");
			PreparedStatement st = conn.prepareStatement(
					"SELECT emp_no, first_name, last_name "
					+ "FROM employees WHERE last_name LIKE ?")) {
			while (!lastName.equals("")) {
				System.out.println("Cognom a cercar (en blanc per sortir): ");
				lastName = sc.nextLine();
				if (!lastName.equals("")) {
					st.setString(1, lastName);
					try (ResultSet rs = st.executeQuery()) {
						while (rs.next()) {
							int empNo = rs.getInt("emp_no");
							String firstName = rs.getString("first_name");
							lastName = rs.getString("last_name");
							System.out.println(empNo+"\t"+firstName+" "+lastName);
						}
					}
				}
			}
		} catch (SQLException e) {
			System.err.println("Error SQL: "+e.getMessage());
		}
		sc.close();
	}
}
```

Hi ha només algunes diferències:

- Hem creat una *PreparedStatement* amb el mètode *prepareStatement()* de la
connexió.

- La sentència SQL a executar es passa a *prepareStatement()*, posant
interrogants als llocs on volem posar els paràmetres.

- Abans d'executar la sentència, utilitzem les diferents variants de *set* que
té *PreparedStatement* (com *setString()*, *setInt()*, etc.) per assignar un
valor concret als paràmetres. El primer integorrant és el paràmetre número 1.

- Finalment, utilitzem el mètode *executeQuery()* del *PreparedStatement* per
obtenir els resultats de la sentència.

- La *PreparedStatement* es pot executar múltiples vegades, modificant els
seus paràmetres, sense haver de tornar a crear una consulta nova.

Si ara intentem passar la consulta problemàtica d'abans, veiem que no
obtenim cap resultat:

```
Cognom a cercar (en blanc per sortir):
' or '1'='1
Cognom a cercar (en blanc per sortir):
```

Un detall que no té a veure amb les sentències preparades però que val la pena
recordar és la diferència entre utilitzar *LIKE* o *=* per comparar cadenes.
En el nostre exemple, com que utilitzem *LIKE*, l'usuari pot introduir cadenes
amb comodins, com *%ders*, obtenint tots els empleats el cognom dels quals
acaba en *ders*.

Si no volem això, com en el cas de l'usuari i contrasenya, que han de ser
exactes, és millor utilitzar la comparació amb *=*, que no admet comodins.

Cal tenir en compte que quan volem fer servir els comodins en una
condició *LIKE*, si els posem directament dins de la cadena,
(`columna LIKE '%?%'`) no ens funcionarà. En aquest cas, tant els
percentatges com l'interrogant s'interpreten com a literals i, per tant,
els mètodes de *PreparedStatement*, com *setString()*, ens llençaran una
excepció quan no trobin els interrogants que esperaven.

Per evitar això podem generar la *PreparedStatement* sense els comodins i
afegir-los posteriorment a la cadena mitjançant el mètode *setString()*.
