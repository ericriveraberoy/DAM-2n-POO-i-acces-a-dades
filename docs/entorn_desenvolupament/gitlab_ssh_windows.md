Configuració per accedir al GitLab amb ssh des de Windows
------------------------

Primer hem de fer un fork del projecte mestre al nostre Gitlab.

Després, accedim a la **consola de git bash.**

[Video tutorial](https://about.gitlab.com/2014/03/04/add-ssh-key-screencast/)

***Comandaments:***

```bash
ssh-keygen -t rsa -C "nombrecorreo"  # Això genera la parella de claus
cat ~/.ssh/id_rsa.pub  # Mostra per pantalla la clau pública
```

La ruta per la parella de claus la pot decidir cada usuari.

```git
git clone git@gitlab.com:NomCompte/RutaRepositori.git
```

**Exemple**: *git@gitlab.com:juanmg2/DAM-2n-POO-i-acces-a-dades.git*

Si en intentar clonar el repositori obtenim el següent error (molt freqüent):
```bash
Permission denied (publickey).
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.
```
Vol dir que no tenim afegida la nostra clau privada a l'agent ssh. 
Per fer-ho primer haurem d'activar l'agent ssh:

```bash
eval $(ssh agent)
```
Si ha anat bé obtindrem un missatge semblant a aquest:
```bash
Agent pid 5600
```

Finalment haurem d'introduir la nostra clau privada a l'agent:
```bash
ssh-add ruta_clau_privada
```
**Exemple**: *~/.ssh/gitlab*

Si ha anat bé obtindrem un missatge semblant a aquest:
```bash
Identity added: gitlab (gitlab)
```
