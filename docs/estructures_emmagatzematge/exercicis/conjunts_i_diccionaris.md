#### Exercicis de conjunts i diccionaris

##### Exercici 1

Utilitza la classe *HashSet* per fer un programa que rebi una línia
de text per l'entrada estàndard i que digui quantes vegades es
repeteix cadascuna de les paraules.

**Exemple**:

Per l'entrada:

`Això és una línia de text de prova, prova és això, una línia i només una línia.`

Sortida:

```
AIXÒ (2)
ÉS (2)
UNA (3)
LÍNIA (3)
DE (2)
TEXT (1)
PROVA (2)
I (1)
NOMÉS (1)
```

*Nota*: Per comptar quants cops apareix una paraula no necessitem un conjunt.
El conjunt s'utilitza per evitar mostrar informació sobre la mateixa paraula
més d'un cop.

##### Exercici 2

Fes un programa que demani dos nombres a l'usuari, i que mostri:

* Nombres del 2 al 1000 que siguin divisors tant del primer nombre
com del segon (sense repetir nombres).

* Nombres del 2 al 1000 que siguin divisors del primer nombre o del
segon (sense repetir nombres).

* Nombres del 2 al 100 que no siguin divisors ni del primer nombre
ni del segon (sense repetir nombres).

Els nombres s'han de mostrar ordenats de menor a major.

*Nota*: Per fer-ho, guardar els divisors d'un dels nombres en un conjunt, i els
divisors de l'altre nombre en un altre conjunt. A partir d'aquí pots calcular
cadascun dels apartats fent operacions de conjunts.

##### Exercici 3

La nostra llista de la compra és:

`6 tomàquets, 4 flams, 2 pizzes, 2 llaunes de tonyina, 5 de blat
de moro i 1 enciam.`

a) Crea un diccionari amb aquestes dades.

b) A partir del diccionari, imprimeix només la llista d'ingredients.

c) Imprimeix la quantitat de tomàquets.

d) Imprimeix aquells ingredients dels quals n'hàgim de comprar més
de 3 unitats.

e) Pregunta a l'usuari un ingredient i retorna el número d'unitats
que s'han de comprar. Si l'ingredient no és a la llista de la
compra, retorna un missatge d'error.

f) Imprimeix tota la llista amb aquest format:

`ingredient (unitats a comprar)`

g) A partir del diccionari, sumar totes les quantitats i donar el
número total d'ítems que hem de comprar. Resultat: 20.

h) Pregunta a l'usuari un ingredient, després demana-li un nou
valor d'unitats i modifica l'entrada corresponent al diccionari.

##### Exercici 4

Partim d'una llista de noms de programa i la seva versió (que
podem codificar directament en el codi del programa):

```
Firefox, 3.5
Premiere, CS3
After Effects, CS3
Photoshop, CS4
Gimp, 2.0.1
Python, 2.6.2
SnagIt, 7.0.1
```

a) Posa aquesta informació en un diccionari i imprimeix-la per
pantalla.

b) Presenta un menú a l'usuari que contingui aquestes tres opcions:

```
1. Mostrar llista
2. Consultar versió
3. Afegir un nou programa
4. Sortir
```

En l'opció 3, cal comprovar que el programa no existeixi al
diccionari.

Si existeix, ha de mostrar un missatge. Si no existeix, s'ha
d'afegir al diccionari.

El programa ha de ser un bucle infinit que només acaba si l'usuari
tria l'opció 4 de *Sortir*.

##### Exercici 5

Emmagatzema les següents parelles de país i capital en una
estructura adequada:

```
Espanya – Madrid
França – París
Itàlia – Roma
Anglaterra – Londres
Alemanya – Berlín
```

Fes un programa que demani un nom a l'usuari i

a) Si aquest és un país pel qual sap la capital, li torni la seva
capital.

b) Si no és un país pel qual sap la capital, demani la capital i
afegeixi aquesta informació. Si l'usuari introdueix una cadena en
blanc com a capital, la informació no s'afegirà.

c) Fes que repeteixi la pregunta fins que la cadena introduïda com
a país estigui en blanc.

```
Introdueix un nom de país: itàlia
La capital de Itàlia és Roma
Introdueix un nom de país: espanya
La capital de Espanya és Madrid
Introdueix un nom de país: grècia
Quina és la capital de Grècia? atenes
Introdueix un nom de país: grècia
La capital de Grècia és Atenes
Introdueix un nom de país: aaa
Quina és la capital de Aaa?
Introdueix un nom de país: aaa
Quina és la capital de Aaa?
Introdueix un nom de país:
```
