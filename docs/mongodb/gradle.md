## Introducció a Gradle

### Introducció

El *Gradle* és un sistema que s'utilitza per automatitzar el procés de
construir (*build*) projectes per Android i Java en general.

A mida que un projecte té més dependències externes, ha de crear diversos
executables o paquets, documentació, o el seu procés d'empaquetat es
complica per qualsevol motiu, surt més a compte utilitzar un sistema que
automatitzi aquestes tasques.

Hi ha diversos sistemes per fer això en Java, com l'*ant* i el *maven*, i
*gradle* és potser el més avançat actualment.

L'ús que en farem nosaltres és dels més senzills possibles: només li
indicarem una dependència del nostre projecte a un fitxer *jar* extern. El
*gradle* s'encarregarà de comprovar si ja el tenim i, si no, baixar-lo.
També l'inclourà al *classpath* de Java per tal que el nostre programa el
tingui disponible.

### Instal·lació del plugin per a l'Eclipse

Per utilitzar *gradle* des d'Eclipse hem d'instal·lar un dels plugins que ens
permet fer-ho. Treballarem amb el *Buildship Gradle Integration*.

Podem instal·lar-ho anant a *Help*->*Eclipse Marketplace* i cercant *gradle*.
Localitzem el plugin i premem *Install*.

Per crear un projecte que utilitzi *Gradle* anem a *File*->*New project...*
i seleccionem *Gradle Project* dins de la carpeta *Gradle*. En tenim prou
amb indicar un nom pel projecte, ja que no cal modificar cap de les altres
opcions que ens vénen.

### Importació del driver de MongoDB utilitzant gradle

L'assistent crea un fitxer anomenat *build.gradle* que és el fitxer de
configuració de *gradle* per al nostre projecte.

Dins de la secció *dependencies* podem afegir les dependències que tingui el
nostre projecte.

Sovint no sabrem exactament quina cadena hem de posar aquí, així que podem
buscar-la. Un bon lloc és http://search.maven.org/. Si cerquem aquí
*mongodb* veurem una colla de resultats. El que ens interessa és el que posa
*mongodb-driver*, que és el *driver* que necessitem per tal que el nostre
programa Java pugui connectar-se al servidor de MongoDB.

Si cliquem sobre de la versió que hi ha (la 3.2.1 en el moment d'escriure
això) i després, a la caixa de l'esquerra que es diu *Dependency information*
seleccionem *gradle*, veurem exactament la línia que hem d'afegit a les
dependències per importar aquesta biblioteca.

La línia en qüestió és `compile 'org.mongodb:mongodb-driver:3.2.1'`. La
copiem dins de la secció *dependencies* de *build.gradle*.

Un cop fet això, n'hi ha prou amb seleccionar el projecte i prémer F5 per tal
que s'actualitzin les dependències. Amb això, el *gradle* es baixarà el
connector i l'incorporarà al nostre projecte.
