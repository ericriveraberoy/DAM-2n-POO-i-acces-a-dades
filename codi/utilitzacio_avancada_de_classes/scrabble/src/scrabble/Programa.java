package scrabble;

import java.util.Arrays;

public class Programa {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String[] paraules = {"prova", "paraules", "java", "hola", "test"};
		Scrabble[] sc = new Scrabble[5];
		Casella[] caselles = new Casella[5];
		ScrabbleEx scx;
		int i;

		for (i=0; i<paraules.length; i++)
			sc[i] = new Scrabble(paraules[i]);

		caselles[0] = new Casella('P');
		caselles[1] = new Casella('R', 2, 1);
		caselles[2] = new Casella('O');
		caselles[3] = new Casella('V', 1, 2);
		caselles[4] = new Casella('A');
		scx = new ScrabbleEx(caselles);

		System.out.println(Arrays.toString(paraules));
		Arrays.sort(paraules, ComparadorScrabble.getInstance());
		System.out.println(Arrays.toString(paraules));
		Arrays.sort(sc);
		System.out.println(Arrays.toString(sc));
		System.out.println(scx.valor());

	}

}
