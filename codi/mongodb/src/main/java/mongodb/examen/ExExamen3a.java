package mongodb.examen;

import java.util.Arrays;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Sorts;

public class ExExamen3a {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("test");
		MongoCollection<Document> coll = db.getCollection("images");

		/*
		db.images.aggregate([
			{$unwind:"$tags"},
			{$group:{_id:"$tags", count:{$sum:1}}},
			{$sort:{count:-1}},
			{$limit:1}])
		*/
		
		Document tag = coll.aggregate(Arrays.asList(
			Aggregates.unwind("$tags"),
			Aggregates.group("$tags", Accumulators.sum("count", 1)),
			Aggregates.sort(Sorts.descending("count")),
			Aggregates.limit(1)
		)).first();
		
		System.out.println("L'etiqueta més utilitzada és "+tag.getString("_id")+" i s'ha utlitzat "+tag.getInteger("count"));
		
		client.close();
	}

}
