package exercicis_raf;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class ModificarPaisos {
	public static final int RECORD_SIZE = 174;
	
	private Scanner sc;
	private int index;
	
	private static String readChars(RandomAccessFile fitxer, int nChars) throws IOException {
		StringBuilder b = new StringBuilder();
		char ch = ' ';
		for (int i=0; i<nChars; i++) {
			ch=fitxer.readChar();
			if (ch != '\0')
				b.append(ch);
		}
		return b.toString();
	}
	
	private void printCountry(RandomAccessFile file) throws IOException {
		Pais p;
		String nom, capital, codiISO;
		int poblacio;
		
		file.seek(index*RECORD_SIZE);
		System.out.println("País: "+file.readInt());
		nom = readChars(file, 40);
		codiISO = readChars(file, 3);
		capital = readChars(file, 40);
		poblacio = file.readInt();
		p = new Pais(nom, codiISO, capital);
		p.setPoblacio(poblacio);
		System.out.println(p);
	}
	
	private void setName(RandomAccessFile file) throws IOException {
		System.out.println("Nou nom: ");
		String nom = sc.nextLine();
		StringBuilder b = new StringBuilder(nom);
		b.setLength(40);
		file.seek(index*RECORD_SIZE+4);
		file.writeChars(b.toString());
	}
	
	private void setCode(RandomAccessFile file) throws IOException {
		System.out.println("Nou codi: ");
		String code = sc.nextLine();
		while (code.length()!=3) {
			System.out.println("El codi ha de ser de tres lletres.");
			code = sc.nextLine();
		}
		file.seek(index*RECORD_SIZE+4+80);
		file.writeChars(code);
	}
	
	private void setCapital(RandomAccessFile file) throws IOException {
		System.out.println("Nova capital: ");
		String nom = sc.nextLine();
		StringBuilder b = new StringBuilder(nom);
		b.setLength(40);
		file.seek(index*RECORD_SIZE+4+80+6);
		file.writeChars(b.toString());
	}
	
	private void setPopulation(RandomAccessFile file) throws IOException {
		int pop;
		System.out.println("Nova població");
		while (!sc.hasNextInt() || (pop=sc.nextInt())<0) {
			System.out.println("La població ha de ser un nombre positiu.");
			sc.nextLine();
		}
		sc.nextLine();
		file.seek(index*RECORD_SIZE+4+80+6+80);
		file.writeInt(pop);
	}
	
	public void run() {
		int nData;
		sc = new Scanner(System.in);
		System.out.println("Introdueix l'índex del país a modificar: ");
		while (!sc.hasNextInt()) {
			System.out.println("Cal introduir un nombre enter.");
			sc.nextLine();
		}
		index = sc.nextInt()-1;
		sc.nextLine();
		try (RandomAccessFile file = new RandomAccessFile("paisos.dat", "rw")) {
			long fileSize = file.length();
			long nRecords = fileSize / RECORD_SIZE;
			if (index<0 || index>=nRecords) {
				System.out.println("Aquest registre no existeix.");
			} else {
				printCountry(file);
				System.out.println("\nDada a modificar:\n1- nom\n2- codi\n3- població\n4- capital\n");
				while (!sc.hasNextInt() || (nData=sc.nextInt())<1 || nData>4) {
					System.out.println("Cal un nombre entre 1 i 4.");
					sc.nextLine();
				}
				sc.nextLine();
				switch (nData) {
				case 1:
					setName(file);
					break;
				case 2:
					setCode(file);
					break;
				case 3:
					setPopulation(file);
					break;
				case 4:
					setCapital(file);
					break;
				}
				System.out.println("Dada modificada.");
				printCountry(file);
			}
		} catch (IOException e) {
			System.out.println("Error en l'accés al fitxer: "+e.getMessage());
		}
		sc.close();
	}

	public static void main(String[] args) {
		(new ModificarPaisos()).run();
	}

}
