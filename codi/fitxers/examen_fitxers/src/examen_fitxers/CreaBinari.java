package examen_fitxers;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CreaBinari {
	public static void main(String[] args) {
		int[] nums = {5, 0, 3, -1, 1234567, 876535, -876443, 1};
		try (DataOutputStream dos = new DataOutputStream(new FileOutputStream("enters.bin"))) {
			for (int n : nums) {
				dos.writeInt(n);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
