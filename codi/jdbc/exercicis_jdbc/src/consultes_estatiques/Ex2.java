package consultes_estatiques;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/*
 * 2. Fes un programa que mostri el nom i cognom dels 
 * actors que apareixen a la pel·lícula TWISTED PIRATES.
 */
public class Ex2 {

	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/sakila";
		String user = "root";
		String passwd = "usbw";
		String sql = "SELECT first_name, last_name FROM actor" +
				" JOIN film_actor USING(actor_id)" +
				" JOIN film USING (film_id)" +
				" WHERE title LIKE 'TWISTED PIRATES'";
		try (Connection connexio = DriverManager.getConnection(url, user, passwd);
				Statement statement = connexio.createStatement();
				ResultSet rs = statement.executeQuery(sql);) {
			while (rs.next())
				System.out.println("Nom i Cognoms: "+rs.getString(1)+" "+rs.getString(2));
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}
}
