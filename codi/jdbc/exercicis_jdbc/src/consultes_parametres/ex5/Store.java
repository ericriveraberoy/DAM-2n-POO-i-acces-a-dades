package consultes_parametres.ex5;

public class Store {
	public final int id;
	public final String address;
	
	public Store(int id, String address) {
		this.id = id;
		this.address = address;
	}
}
