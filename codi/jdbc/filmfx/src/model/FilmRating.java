package model;

public enum FilmRating {
	G, PG, PG13, R, NC17;
}
