package exsllistes;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Ex1Llistes {
	public static void main(String[] args) {
		List<String> llista = new ArrayList<String>(); // seria més ràpid amb un
														// conjunt
		String cadena = " ";
		Scanner scanner = new Scanner(System.in);

		while (!cadena.equals("")) {
			System.out.println("Introdueix una cadena (enter per acabar): ");
			cadena = scanner.nextLine();
			if (!cadena.equals(""))
				llista.add(cadena);
		}

		cadena = " ";
		while (!cadena.equals("")) {
			System.out.println("Quina cadena vols comprovar? ");
			cadena = scanner.nextLine();
			if (!cadena.equals(""))
				if (llista.contains(cadena))
					System.out.println(cadena + " s'ha introduït.");
				else
					System.out.println(cadena + " no s'ha introduït.");
		}
		scanner.close();
	}
}
