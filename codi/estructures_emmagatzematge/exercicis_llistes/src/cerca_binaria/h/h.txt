Per què el mètode sort no està definit a la interfície List, o a les classes ArrayList
i LinkedList, i s'ha implementat com a mètode estàtic de la classe Collections?

- Perquè les llistes, en general, no tenen perquè contenir objectes comparables entre ells, i
això és una condició necessària per tal que es puguin ordenar.